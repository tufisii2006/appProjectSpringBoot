package com.application.appProject;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.application.appProject.abstractModel.Branch;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Creator;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.validators.VehicleValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleValidatorTest {

	@Test
	public void testIsInputStringLengthValidWhenValidStringReturnTrue() {
		String testString = "string1234";
		assertTrue(VehicleValidator.isInputStringLengthValid(testString, 10));
	}

	@Test
	public void testIsInputStringLengthValidWhenValidStringReturnFalse() {
		String testString = "string1234";
		assertFalse(VehicleValidator.isInputStringLengthValid(testString, 20));
	}

	@Test
	public void testIsVehicleIdValidWhenValidIdReturnTrue() {
		String fin = "fin";
		Branch branch = Branch.CAR;
		Brand brand = Brand.MERCEDES;
		String id = "fin0095";
		assertTrue(VehicleValidator.isVehicleIdValid(id, fin, branch, brand));
	}

	@Test
	public void testIsVehicleIdValidWhenValidIdReturnFalse() {
		String fin = "fin";
		Branch branch = Branch.CAR;
		Brand brand = Brand.MERCEDES;
		String id = "0095fin";
		assertFalse(VehicleValidator.isVehicleIdValid(id, fin, branch, brand));
	}

	@Test
	public void testIsVehicleFinValidWhenValidFinReturnTrue() {
		String fin = "897findfgehrutrue";
		assertTrue(VehicleValidator.isVehicleFinValid(fin));
	}

	@Test
	public void testIsVehicleFinValidWhenValidFinReturnFalse() {
		String fin = "fals897findfgehrdgfdgfdgfdgfufalse";
		assertFalse(VehicleValidator.isVehicleFinValid(fin));
	}

	@Test
	public void testIsVehicleValidWhenValidVehicleReturnTrue() {
		MongoVehicle vehicle = new MongoVehicle();
		vehicle.setBranch(Branch.CAR);
		vehicle.setBrand(Brand.MERCEDES);
		vehicle.setFin("897findfgehrutrue");
		vehicle.setVehicleClass("Clasa1");
		vehicle.setCreatedBy(Creator.IOANA);
		vehicle.setId("897findfgehrutrue0095");
		vehicle.setModelYear("123");
		vehicle.setTransmissionType("transmison");
		vehicle.setEngineType("enginetype");
		vehicle.setEmissionStandard("emission12");
		vehicle.setCommisionNumber("commision12345");
		assertTrue(VehicleValidator.isVehicleValid(vehicle));
	}

	@Test
	public void testIsVehicleValidWhenValidVehicleReturnFalse() {
		MongoVehicle vehicle = null;
		assertFalse(VehicleValidator.isVehicleValid(vehicle));
	}
}
