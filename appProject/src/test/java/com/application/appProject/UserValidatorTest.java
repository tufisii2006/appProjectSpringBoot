package com.application.appProject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.validators.UserValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserValidatorTest {

	@Test
	public void testIsEmailValidWhenValidEmailReturnTrue() {
		assertTrue(UserValidator.isUserEmailAddressValid("radu.tufisi@fortech.ro"));
	}

	@Test
	public void testIsEmailValidWhenInvalidEmailReturnFalse() {
		assertFalse(UserValidator.isUserEmailAddressValid("radu.tufisi@forte"));
	}

	@Test
	public void testIsCnpValidWhenValidCnpReturnTrue() {
		assertTrue(UserValidator.isUserCnpValid("1943434564954"));
	}

	@Test
	public void testIsCnpValidWhenInvalidCnpReturnFalse() {
		assertFalse(UserValidator.isUserCnpValid("19454"));
	}

	@Test
	public void testIsUserPasswordValidWhenValidPasswordReturnTrue() {
		assertTrue(UserValidator.isUserPasswordValid("mkyong1A@"));
	}

	@Test
	public void testIsUserPasswordValidWhenInvalidPasswordReturnFalse() {
		assertFalse(UserValidator.isUserPasswordValid("notgoodpass"));
	}

	@Test
	public void testIsUserValidWhenValidUserReturnTrue() {
		MongoUser testUser = new MongoUser();
		testUser.setFirstName("Tufisi");
		testUser.setLastName("Radu Gabriel");
		testUser.setPhone("0740056450");
		testUser.setPassword("tufaParola.23f");
		testUser.setUsername("tufiUsername");
		testUser.setEmail("radu.tufisi@fortech.ro");
		testUser.setCnp("1950929385640");
		assertTrue(UserValidator.isUserValid(testUser));
	}

	@Test
	public void testIsUserValidWhenInvalidUserReturnFalse() {
		MongoUser testUser = new MongoUser();
		testUser.setFirstName("Tufisi");
		testUser.setLastName("Radu Gabriel");
		testUser.setPhone("0740056450");
		testUser.setPassword("tufaPar");
		testUser.setUsername("tufiUsername");
		testUser.setEmail("radu.tufisMail");
		testUser.setCnp("195092640");
		assertFalse(UserValidator.isUserCnpValid(testUser.getCnp()));
		assertFalse(UserValidator.isUserEmailAddressValid(testUser.getEmail()));
		assertFalse(UserValidator.isUserPasswordValid(testUser.getPassword()));
		assertFalse(UserValidator.isUserValid(testUser));
	}

}
