package com.application.appProject;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.application.appProject.abstractModel.State;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.validators.ContractValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractValidatorTest {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Test
	public void testIsContractRvgValidWhenValidRvgReturnTrue() {
		double validRvg = 12.23;
		assertTrue(ContractValidator.isContractRvgValid(validRvg));
	}

	@Test
	public void testIsContractRvgValidWhenInvalidRvgReturnFalse() {
		double invalidRvg = 122.2223;
		assertFalse(ContractValidator.isContractRvgValid(invalidRvg));
	}

	@Test
	public void testIsContractValidToBeCanceledWhenItIsActiveValidDateReturnTrue() throws ParseException {
		MongoContract contract = new MongoContract();
		contract.setState(State.ACTIVE);
		contract.setStartDate(new SimpleDateFormat("yyyy-MM-dd").parse("2015-07-12"));
		assertTrue(ContractValidator.isContractValidToBeCanceledWhenItIsActive(contract.getStartDate()));
	}

	@Test
	public void testIsContractValidToBeCanceledWhenItIsActiveInvalidDateReturnFalse() throws ParseException {
		MongoContract contract = new MongoContract();
		contract.setState(State.ACTIVE);
		contract.setStartDate(new SimpleDateFormat("yyyy-MM-dd").parse("2019-07-12"));
		assertFalse(ContractValidator.isContractValidToBeCanceledWhenItIsActive(contract.getStartDate()));
	}

	@Test
	public void testIsContractValidToChangeStateFromIn_PreparationToCanceledWhenValidContractReturnTrue() {
		MongoContract contract = new MongoContract();
		contract.setState(State.IN_PREPARATION);
		contract.setNumber("Number1212");
		assertTrue(ContractValidator.isContractValidToChangeStateFromIn_PreparationToCanceled(contract));
	}

	@Test
	public void testIsContractValidToChangeStateFromIn_PreparationToCanceledWhenNullContractThrowsException() {
		MongoContract contract = null;
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The contract is null!");
		assertTrue(ContractValidator.isContractValidToChangeStateFromIn_PreparationToCanceled(contract));
	}

	@Test
	public void testIsContractValidToChangeStateFromIn_PreparationToCanceledWhenEmptyContractNumberThrowsException() {
		MongoContract contract = new MongoContract();
		contract.setNumber(StringUtils.EMPTY);
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The contract number is null or invalid!");
		assertTrue(ContractValidator.isContractValidToChangeStateFromIn_PreparationToCanceled(contract));
	}

	@Test
	public void testIsContractValidToChangeStateFromIn_PreparationToCanceledWhenInvalidContractStateThrowsException() {
		MongoContract contract = new MongoContract();
		contract.setState(State.ACTIVE);
		thrown.expect(IllegalArgumentException.class);
		assertTrue(ContractValidator.isContractValidToChangeStateFromIn_PreparationToCanceled(contract));
	}

	@Test
	public void testIsContractValidWhenValidContractReturnTrue() {
		MongoContract contract = new MongoContract();
		contract.setState(State.ACTIVE);
		contract.setMileagePerYear(2);
		contract.setRvg(BigDecimal.valueOf(12.3));
		contract.setNumber("Number1234");
		contract.setVehicleFin("Fin1234323");
		contract.setRateSubvention(BigDecimal.valueOf(34.23));
		contract.setRuGuarantor("RuGuaranto");
		assertTrue(ContractValidator.isContractValid(contract));
	}
	@Test
	public void testIsContractValidWhenNullContractReturnFalse() {
		MongoContract contract = null;
		assertFalse(ContractValidator.isContractValid(contract));
	}

}
