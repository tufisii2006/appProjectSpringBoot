package com.application.appProject.mockTest;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;

import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.mongoRepository.UserMongoRepository;
import com.application.appProject.securityUtils.UserAlreadyExistsException;
import com.application.appProject.service.UserServiceMongoImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Mock
	private UserMongoRepository userRepo;

	@InjectMocks
	private UserServiceMongoImpl userService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testUpdateUserPasswordWhenValidUserCnp()
			throws IllegalArgumentException, NoSuchElementException, NoSuchAlgorithmException {
		MongoUser user = new MongoUser();
		user.setCnp("19509");
		user.setPassword("firstPassword");
		when(userRepo.findByCnp("19509")).thenReturn(user);
		userService.updateUserPassword(user);
		verify(userRepo).save(user);
	}

	@Test
	public void testUpdateUserPasswordWhenInvalidUserCnpThrowException() throws Exception {
		MongoUser user = new MongoUser();
		user.setCnp(StringUtils.EMPTY);
		thrown.expect(IllegalArgumentException.class);
		userService.updateUserPassword(user);
	}

	@Test
	public void testGetAllUsersFromDbWhenThereExistUsersInDb() {
		List<MongoUser> testAllUsers = new LinkedList<MongoUser>();
		testAllUsers.add(new MongoUser());
		testAllUsers.add(new MongoUser());
		when(userRepo.findAll()).thenReturn(testAllUsers);
		userService.getAllUsersFromDb();
		assertNotNull(testAllUsers);
	}

	@Test
	public void testGetAllUsersFromDbWhenNoUserInsertedInDb() {
		List<MongoUser> testAllUsers = new LinkedList<MongoUser>();
		when(userRepo.findAll()).thenReturn(testAllUsers);
		thrown.expect(EmptyResultDataAccessException.class);
		userService.getAllUsersFromDb();
	}

	@Test
	public void testGetOneUserByCnpWhenValidCnp() {
		MongoUser user = new MongoUser();
		user.setCnp("1123");
		when(userRepo.findByCnp("1123")).thenReturn(user);
		assertNotNull(userService.getOneUserByCnp("1123"));

	}

	@Test
	public void testGetOneUserByCnpWhenInvalidCnpThrowsException() {
		MongoUser user = new MongoUser();
		user.setCnp(StringUtils.EMPTY);
		thrown.expect(IllegalArgumentException.class);
		userService.getOneUserByCnp(user.getCnp());
	}

	@Test
	public void testDeleteUserByCnpWhenValidCnp() {
		MongoUser user = new MongoUser();
		user.setCnp("1123");
		when(userRepo.findByCnp("1123")).thenReturn(user);
		userService.deleteUserByCnp(user.getCnp());
		verify(userRepo).delete(user);
	}

	@Test
	public void testDeleteUserByCnpWhenUserNotFoundThrowException() {
		MongoUser user = new MongoUser();
		user.setCnp("1123");
		when(userRepo.findByCnp(Mockito.anyString())).thenReturn(null);
		thrown.expect(NoSuchElementException.class);
		userService.deleteUserByCnp(user.getCnp());
	}

	@Test
	public void testInsertUserWhenValidUser() throws IllegalArgumentException,UserAlreadyExistsException {
		MongoUser testUser = new MongoUser();
		testUser.setFirstName("Tufisi");
		testUser.setLastName("Radu Gabriel");
		testUser.setPhone("0740056450");
		testUser.setPassword("tufaParola.23f");
		testUser.setUsername("tufiUsername");
		testUser.setEmail("radu.tufisi@fortech.ro");
		testUser.setCnp("1950929385640");
		userService.insertUser(testUser);
		verify(userRepo).save(testUser);
	}

	@Test
	public void testInsertUserWhenNullUserThrowsException() throws IllegalArgumentException,UserAlreadyExistsException {
		MongoUser testUser = null;
		thrown.expect(IllegalArgumentException.class);
		userService.insertUser(testUser);

	}
}
