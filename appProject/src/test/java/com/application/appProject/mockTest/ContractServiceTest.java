package com.application.appProject.mockTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.application.appProject.abstractModel.State;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.mongoRepository.ContractRepository;
import com.application.appProject.mongoRepository.UserMongoRepository;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.service.ContractServiceImpl;
import com.application.appProject.service.UserServiceMongoImpl;

@RunWith(MockitoJUnitRunner.class)
public class ContractServiceTest {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Mock
	private ContractRepository contractRepo;

	@Mock
	private UserServiceMongoImpl userService;

	@Mock
	private UserMongoRepository userRepo;

	@InjectMocks
	private ContractServiceImpl contractService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testContractCancelationWhenContractIsActiveInvalidDateThrowsException() throws ParseException {
		MongoContract contract = new MongoContract();
		contract.setState(State.ACTIVE);
		contract.setContractVersion(1);
		contract.setNumber("123");
		contract.setStartDate(new SimpleDateFormat("yyyy-MM-dd").parse("2019-07-12"));
		when(contractRepo.findByNumber("123")).thenReturn(contract);
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("A contract can be canceled only if at least 1 year had passed from the start date!");
		contractService.changeContractStateFromActiveToCanceled(contract);
	}

	@Test
	public void testContractCancelationWhenContractIsActiveReturnTrue() throws ParseException {
		MongoContract contract = new MongoContract();
		contract.setState(State.ACTIVE);
		contract.setContractVersion(1);
		contract.setNumber("123");
		contract.setStartDate(new SimpleDateFormat("yyyy-MM-dd").parse("2015-07-12"));
		when(contractRepo.findByNumber("123")).thenReturn(contract);
		contractService.changeContractStateFromActiveToCanceled(contract);
		verify(contractRepo).save(contract);
		assertNull(contract.getModifiedByUserCnp());
		assertNotNull(contract.getCancelationDate());
	}

	@Test
	public void testChangeContractStateFromIn_PreparationToCancelWhenValidContractInput() {
		MongoContract contract = new MongoContract();
		contract.setNumber("NrTest");
		contract.setState(State.IN_PREPARATION);
		contract.setContractVersion(1);
		when(contractRepo.findByNumber("NrTest")).thenReturn(contract);
		contractService.changeContractStateFromIn_PreparationToCanceled(contract);
		assertNotNull(contract.getCancelationDate());
		assertEquals(contract.getState(), State.CANCELED);
		assertEquals(contract.getContractVersion(), Integer.valueOf(2));
		verify(contractRepo).save(contract);
	}

	@Test
	public void testDeleteContractByNumberWhenValidContractInput() {
		MongoContract contract = new MongoContract();
		contract.setNumber("NrTest");
		when(contractRepo.findByNumber("NrTest")).thenReturn(contract);
		contractService.deleteContractByNumber(contract.getNumber());
		verify(contractRepo).delete(contract);
	}

	@Test
	public void testDeleteContractByNumberWhenInvalidContractNumberInputThrowsException() {

		MongoContract contract = new MongoContract();
		contract.setNumber("NrTest");
		when(contractRepo.findByNumber("NrTest")).thenReturn(null);
		thrown.expect(NoSuchElementException.class);
		thrown.expectMessage("No contract having that number was found!");
		contractService.deleteContractByNumber(contract.getNumber());

	}

	@Test
	public void testDeleteContractByNumberWhenContractNumberIsNullThrowsException() {
		MongoContract contract = new MongoContract();
		contract.setNumber(StringUtils.EMPTY);
		thrown.expect(IllegalArgumentException.class);
		contractService.deleteContractByNumber(contract.getNumber());
	}

	@Test
	public void testActivateExistingContractWhenValidContract() {
		MongoContract contract = new MongoContract();
		MongoUser user = new MongoUser();
		user.setCnp("1923");
		contract.setNumber("testNr");
		contract.setState(State.IN_PREPARATION);
		contract.setCreatedByUserCnp(user.getCnp());
		when(userService.getOneUserByCnp(contract.getCreatedByUserCnp())).thenReturn(user);
		when(contractRepo.findByNumber("testNr")).thenReturn(contract);
		contractService.activateExistingContract(contract);
		assertEquals(contract.getState(), State.ACTIVE);
		assertEquals(contract.getContractVersion(), Integer.valueOf(2));
		assertNotNull(contract.getStartDate());
		assertNotNull(contract.getModifiedByUserCnp());
		verify(contractRepo).save(contract);
	}

	@Test
	public void testActivateExistingContractWhenInvalidContractStateThrowException() {
		MongoContract contract = new MongoContract();
		contract.setState(State.CANCELED);
		contract.setNumber("NrTest");
		when(contractRepo.findByNumber("NrTest")).thenReturn(contract);
		thrown.expect(IllegalArgumentException.class);
		contractService.activateExistingContract(contract);
	}

	@Test
	public void testActivateExistingContractWhenInvalidContractNumberThrowException() {
		MongoContract contract = new MongoContract();
		contract.setState(State.CANCELED);
		contract.setNumber(StringUtils.EMPTY);
		when(contractRepo.findByNumber(Mockito.anyString())).thenReturn(null);
		thrown.expect(NoSuchElementException.class);
		contractService.activateExistingContract(contract);
	}
}
