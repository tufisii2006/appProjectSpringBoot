package com.application.appProject.mockTest;

import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;

import com.application.appProject.abstractModel.Branch;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Creator;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.mongoRepository.ContractRepository;
import com.application.appProject.mongoRepository.VehicleMongoRepository;
import com.application.appProject.service.ContractServiceImpl;
import com.application.appProject.service.VehicleService;
import com.application.appProject.service.VehicleServiceMongoImpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class VehicleServiceTest {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Mock
	private VehicleMongoRepository vehicleRepo;

	@InjectMocks
	private VehicleServiceMongoImpl vehicleService;

	@Test
	public void testGetAllVehicleFromDbWhenThereAreVehiclesInDb() {
		List<MongoVehicle> testallVehicles = new LinkedList<MongoVehicle>();
		testallVehicles.add(new MongoVehicle());
		testallVehicles.add(new MongoVehicle());
		when(vehicleRepo.findAll()).thenReturn(testallVehicles);
		vehicleService.getAllVehiclesFromDb();
		assertNotNull(testallVehicles);
	}

	@Test
	public void testGetAllVehicleFromDbWhenThereAreNotAnyVehiclesInDbThrowException() {
		List<MongoVehicle> testallVehicles = new LinkedList<MongoVehicle>();
		when(vehicleRepo.findAll()).thenReturn(testallVehicles);
		thrown.expect(EmptyResultDataAccessException.class);
		vehicleService.getAllVehiclesFromDb();
	}

	@Test
	public void testGetOneVehicleByFinWhenValidFin() {
		MongoVehicle testVehicle = new MongoVehicle();
		testVehicle.setFin("Fin12");
		when(vehicleRepo.findByFin("Fin12")).thenReturn(testVehicle);
		assertNotNull(vehicleService.getOneVehicleByFin(testVehicle.getFin()));
	}

	@Test
	public void testGetOneVehicleByFinWhenNullFinThrowException() {
		String nullFin = null;
		thrown.expect(IllegalArgumentException.class);
		vehicleService.getOneVehicleByFin(nullFin);
	}

	@Test
	public void testInsertVehicleWhenValidVehicle() {
		MongoVehicle vehicle = new MongoVehicle();
		vehicle.setBranch(Branch.CAR);
		vehicle.setBrand(Brand.MERCEDES);
		vehicle.setFin("897findfgehrutrue");
		vehicle.setVehicleClass("Clasa1");
		vehicle.setCreatedBy(Creator.IOANA);
		vehicle.setId("897findfgehrutrue0095");
		vehicle.setModelYear("123");
		vehicle.setTransmissionType("transmison");
		vehicle.setEngineType("enginetype");
		vehicle.setEmissionStandard("emission12");
		vehicle.setCommisionNumber("commision12345");
		vehicleService.insertVehicle(vehicle);
		verify(vehicleRepo).save(vehicle);
	}

	@Test
	public void testInsertVehicleWhenInvalidVehicleThrowsException() {
		MongoVehicle vehicle = new MongoVehicle();
		vehicle.setModelYear("123");
		vehicle.setTransmissionType("transmison");
		vehicle.setEngineType("enginetype");
		vehicle.setEmissionStandard("emission12");
		vehicle.setCommisionNumber("commision12345");
		thrown.expect(IllegalArgumentException.class);
		vehicleService.insertVehicle(vehicle);
	}

	@Test
	public void testDeleteVehicleFromDbWhenValidFin() {
		MongoVehicle vehicle = new MongoVehicle();
		vehicle.setFin("897findfgehrutrue");
		when(vehicleRepo.findByFin("897findfgehrutrue")).thenReturn(vehicle);
		vehicleService.deleteVehicleFromDb(vehicle.getFin());
		verify(vehicleRepo).delete(vehicle);
	}

	@Test
	public void testDeleteVehicleFromDbWhenNullFinThrowsException() {
		MongoVehicle vehicle = new MongoVehicle();
		vehicle.setFin(null);
		thrown.expect(IllegalArgumentException.class);
		vehicleService.deleteVehicleFromDb(vehicle.getFin());
	}

	@Test
	public void testDeleteVehicleFromDbWhenInvalidFinThrowsException() {
		String badFin= "badFin";
		when(vehicleRepo.findByFin(badFin)).thenReturn(null);
		thrown.expect(NoSuchElementException.class);
		vehicleService.deleteVehicleFromDb(badFin);
	}
}
