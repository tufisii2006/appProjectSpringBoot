package com.application.appProject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.application.appProject.mappers.VehicleMapper;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.restModel.VehicleRequest;
import com.application.appProject.restModel.VehicleResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MapperTest {

	@Test
	public void testVehicleRequestToEntityReturnTrue() {
		VehicleRequest vehicleRequest = new VehicleRequest();
		vehicleRequest.setEmissionStandard("emis");
		vehicleRequest.setModel("BMW");
		MongoVehicle vehicle = VehicleMapper.mapRequestToEntity(vehicleRequest);
		assertEquals(vehicle.getEmissionStandard(), "emis");
		assertEquals(vehicle.getModel(), "BMW");
	}

	@Test
	public void testVehicleRequestToEntityReturnFalse() {
		VehicleRequest vehicleRequest = new VehicleRequest();
		vehicleRequest.setEmissionStandard("emis");
		vehicleRequest.setModel("BMW");
		MongoVehicle vehicle = VehicleMapper.mapRequestToEntity(vehicleRequest);
		assertNotEquals(vehicle.getEmissionStandard(), "emisNot");
		assertNotEquals(vehicle.getModel(), "BMWnotGood");
	}

	@Test
	public void testVehicleEntityToResponseReturnTrue() {
		MongoVehicle vehicle = new MongoVehicle();
		vehicle.setFin("fin");
		VehicleResponse vehicleResponse = VehicleMapper.mapEntityToResponse(vehicle);
		assertEquals("fin",vehicleResponse.getVehicleFin());

	}

	@Test
	public void testVehicleEntityToResponseReturnFalse() {
		MongoVehicle vehicle = new MongoVehicle();
		vehicle.setFin("fin");
		VehicleResponse vehicleResponse = VehicleMapper.mapEntityToResponse(vehicle);
		assertNotEquals(vehicleResponse.getId(), "finaa");

	}

}
