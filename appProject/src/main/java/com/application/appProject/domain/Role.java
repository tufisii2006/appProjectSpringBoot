package com.application.appProject.domain;

public enum Role {
    USER("USER"),
    EXPLORER("EXPLORER"),
    ADMIN("ADMIN");
    
    private final String value;

	Role(final String newValue) {
		value = newValue;
	}

	public String value() {
		return value;
	}

	public static Role fromString(final String roleTypeString) {
		for (final Role roleType : Role.values()) {
			if (roleType.value().equalsIgnoreCase(roleTypeString)) {
				return roleType;
			}
		}
		return null;
	}
}
