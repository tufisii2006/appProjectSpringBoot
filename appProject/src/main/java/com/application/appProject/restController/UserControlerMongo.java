package com.application.appProject.restController;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.jws.soap.SOAPBinding.Use;
import javax.validation.Valid;

import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.abstractModel.User;
import com.application.appProject.domain.Role;
import com.application.appProject.factory.UserRequestFactory;
import com.application.appProject.factory.UserResponseFactory;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.restModel.UserRequest;
import com.application.appProject.restModel.UserRequestUpdatePassword;
import com.application.appProject.restModel.UserResponse;
import com.application.appProject.securityUtils.UserAlreadyExistsException;
import com.application.appProject.service.UserService;
import com.application.appProject.service.UserServiceSQLImpl;
import com.application.appProject.service.UserSessionService;

@RestController
@RequestMapping("/app1")
public class UserControlerMongo {

	@Autowired
	@Qualifier("MongoService")
	private UserService userService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Return a list of all {@link MongoUser} in the Database.The {@link MongoUser}
	 * has to be logged in and has the role either ADMIN or EXPLORER
	 * 
	 * @return ResponseEntity<List<UserResponse>>
	 */
	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@RequestMapping(path = "user/all", method = RequestMethod.GET, produces = { "application/xml", "application/json" })
	public ResponseEntity<List<UserResponse>> getAllUsers() {
		List<User> allUsers = new ArrayList<User>();
		List<UserResponse> allResponseUsers = new ArrayList<UserResponse>();
		try {
			MongoUser sessionUser = UserSessionService.getSessionsUser();
			System.err.println(sessionUser.toString());
			if (StringUtils.equals(sessionUser.getRole(), "ROLE_" + Role.ADMIN.value())) {
				 allUsers = userService.getAllUsersFromDb();
			}
			if (StringUtils.equals(sessionUser.getRole(), "ROLE_" + Role.USER.value())) {
				 allUsers.add(userService.getOneUserByCnp(sessionUser.getCnp()));
			}
			for (User user : allUsers) {
				UserResponse userResponse = UserResponseFactory.getUserResponseInstance(user);
				allResponseUsers.add(userResponse);
			}
			return new ResponseEntity<List<UserResponse>>(allResponseUsers, HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			UserResponse userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			allResponseUsers.add(userResponse);
			return new ResponseEntity<List<UserResponse>>(allResponseUsers, HttpStatus.BAD_REQUEST);

		}
	}

	/**
	 * Handles the request of creating a new {@link Use}.The {@link MongoUser} has
	 * to be logged in and has the role ADMIN
	 * 
	 * @param userRequest
	 * @return ResponseEntity<UserResponse>
	 */
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "user/create/", method = RequestMethod.POST, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<UserResponse> insertUser(@RequestBody UserRequest userRequest) {
		UserResponse userResponse = new UserResponse();
		try {
			User user = new MongoUser();
			user.setFirstName("Tufisi");
			user.setLastName("Radu Gabriel");
			user.setPhone("0740056450");
			user.setPassword("tufaParola.23f");
			user.setUsername("tufiUsername");
			user.setEmail("radu.tufisi@fortech.ro");
			user.setCnp("1950929386666");
			user.setRole(Role.ADMIN.value());
			// UserRequestFactory.getUserInstanceFromUserRequest(userRequest);
			System.err.println(user.toString());
			// User user = UserRequestFactory.getUserInstanceFromUserRequest(userRequest);
			System.err.println(user.toString());
			userService.insertUser(user);
			userResponse = UserResponseFactory.getUserResponseInstance(user);
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (UserAlreadyExistsException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} 

	}

	/**
	 * Handles the request of updating the password of a {@link MongoUser} . The
	 * {@link Use} has to be logged in and has the role ADMIN
	 * 
	 * @param userRequest
	 * @return ResponseEntity<UserResponse>
	 */
	@PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
	@RequestMapping(path = "user/updatePassword/", method = RequestMethod.PATCH, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<UserResponse> updateUserPassword(
			@Valid @RequestBody(required = true) UserRequestUpdatePassword userRequest) {
		UserResponse userResponse = new UserResponse();
		try {
			MongoUser user = (MongoUser) UserRequestFactory.getUserUpdatePasswordInstanceFromUserRequest(userRequest);
			if (StringUtils.equals(UserSessionService.getSessionsUser().getRole(), "ROLE_" + Role.USER.value())) {
				if (user.getCnp() != UserSessionService.getSessionsUser().getCnp()) {
					WebAuthenticationDetails details = UserSessionService.getConnectionDetailsOfSessionUser();
					logger.warn("IMPORTANT WARNING!AN USER WAS TRYING TO UPDATE AN OTHER USER PASSWORD!!!");
					logger.warn("SESSION ID IS : " + details.getSessionId());
					logger.warn("REMOTE ADDRESS : " + details.getRemoteAddress());
					userResponse = UserResponseFactory.getUserResponseErrorInstance("Forbiden!!");
					return new ResponseEntity<UserResponse>(userResponse, HttpStatus.I_AM_A_TEAPOT);
				}
			}
			// userService.updateUserPassword(user);
			userResponse = UserResponseFactory
					.getUserResponseSuccessMessageInstance("Password was changed succesfully!");
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Handles the request of updating the informations of a {@link MongoUser}
	 * (firstname,lastname,email etc). The {@link Use} has to be logged in and has
	 * the role USER
	 * 
	 * @param userRequest
	 * @return ResponseEntity<UserResponse>
	 */
	@PreAuthorize("hasAnyRole('USER')")
	@RequestMapping(path = "user/editInfo/", method = RequestMethod.PATCH, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<UserResponse> updateUserPersonalInformation(
			@Valid @RequestBody(required = true) UserRequest userRequest) {
		UserResponse userResponse = new UserResponse();
		try {
			MongoUser user = (MongoUser) UserRequestFactory.getUserInstanceFromUserRequest(userRequest);
			if (!StringUtils.equals(user.getCnp(), UserSessionService.getSessionsUser().getCnp())) {
				WebAuthenticationDetails details = UserSessionService.getConnectionDetailsOfSessionUser();
				logger.warn("IMPORTANT WARNING!AN USER WAS TRYING TO UPDATE PERSONAL INFORMATIONS OF ANOTHER USER!!!");
				logger.warn("User having CNP : " + UserSessionService.getSessionsUser().getCnp()
						+ " SENDS REQUEST WITH CNP: " + user.getCnp());
				logger.warn("SESSION ID IS : " + details.getSessionId());
				logger.warn("REMOTE ADDRESS : " + details.getRemoteAddress());
				userResponse = UserResponseFactory.getUserResponseErrorInstance("Mind your own data biatch :D !!");
				return new ResponseEntity<UserResponse>(userResponse, HttpStatus.I_AM_A_TEAPOT);
			}

			// userService.updateUserPersonalInfo(user);
			userResponse = UserResponseFactory
					.getUserResponseSuccessMessageInstance("User information were changed succesfully");
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
