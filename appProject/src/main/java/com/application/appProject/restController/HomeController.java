package com.application.appProject.restController;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class HomeController {

	/**
	 * Returns the home page of the application
	 * @return String
	 */
	@ResponseBody
	@RequestMapping("/home")
	public String homePageMessage() {
		return "Servus!Avem o aplicatie mica cu REST Api!";
	}
	
}
