package com.application.appProject.restController;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.jws.soap.SOAPBinding.Use;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.abstractModel.User;
import com.application.appProject.domain.Role;
import com.application.appProject.factory.UserRequestFactory;
import com.application.appProject.factory.UserResponseFactory;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.restModel.UserRequest;
import com.application.appProject.restModel.UserResponse;
import com.application.appProject.securityUtils.UserAlreadyExistsException;
import com.application.appProject.service.UserService;
import com.application.appProject.service.UserServiceSQLImpl;
import com.application.appProject.service.UserSessionService;

@RestController
@RequestMapping("/app2")
public class UserControlerSQL {

	@Autowired
	@Qualifier("SQLService")
	private UserService userService;

	/**
	 * Handles the request of creating a new {@link Use}.The {@link MongoUser} has
	 * to be logged in and has the role ADMIN
	 * 
	 * @param userRequest
	 * @return ResponseEntity<UserResponse> @throws
	 */

	@RequestMapping(path = "user/create", method = RequestMethod.POST, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<UserResponse> insertUser(@Valid @RequestBody UserRequest userRequest) {
		UserResponse userResponse = new UserResponse();
		try {
			ModelMapper modelMapper = new ModelMapper();
			SqlUser user = modelMapper.map(userRequest, SqlUser.class);
			userService.insertUser(user);
			userResponse = UserResponseFactory.getUserResponseInstance(user);
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);

		} catch (UserAlreadyExistsException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(path = "user/one", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<UserResponse> getOneUserByCnp(@RequestBody UserRequest userRequest) {
		UserResponse userResponse = new UserResponse();
		try {

			User user = userService.getOneUserByCnp(userRequest.getCnp());
			userResponse = UserResponseFactory.getUserResponseInstance(user);
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "user/delete", method = RequestMethod.DELETE, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<UserResponse> deleteUser(@RequestBody UserRequest userRequest) {
		UserResponse userResponse = new UserResponse();
		try {
			userService.deleteUserByCnp(userRequest.getCnp());
			userResponse = UserResponseFactory
					.getUserResponseSuccessMessageInstance("User was succesfully deleted from DB!");
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.BAD_REQUEST);
		} catch (NoSuchElementException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(path = "user/all", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<UserResponse>> getAllUsers() {
		List<UserResponse> alluserResponse = new ArrayList<UserResponse>();
		List<User> allUsersFromDb = new ArrayList<User>();
		try {
			allUsersFromDb = userService.getAllUsersFromDb();
			for (User user : allUsersFromDb) {
				UserResponse userResponse = UserResponseFactory.getUserResponseInstance(user);
				alluserResponse.add(userResponse);
			}
			return new ResponseEntity<List<UserResponse>>(alluserResponse, HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			alluserResponse.add(UserResponseFactory.getUserResponseErrorInstance(e.getMessage()));
			return new ResponseEntity<List<UserResponse>>(alluserResponse, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(path = "user/update", method = RequestMethod.PUT, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<UserResponse> updatePersonalInfoOfUser(@RequestBody UserRequest userRequest) {
		UserResponse userResponse = new UserResponse();
		try {
			ModelMapper modelMapper = new ModelMapper();
			SqlUser user = modelMapper.map(userRequest, SqlUser.class);
			userService.updateUserPersonalInfo(user);
			userResponse = UserResponseFactory.getUserResponseInstance(user);
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			userResponse = UserResponseFactory.getUserResponseErrorInstance(e.getMessage());
			return new ResponseEntity<UserResponse>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
