package com.application.appProject.restController;

import java.util.ArrayList;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.factory.VehicleRequestFactory;
import com.application.appProject.factory.VehicleResponseFactory;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.restModel.VehicleRequest;
import com.application.appProject.restModel.VehicleResponse;
import com.application.appProject.service.VehicleService;

@RestController
@RequestMapping("/app1")
public class VehicleControlerMongo {

	@Autowired
	@Qualifier("VehicleServiceMongo")
	private VehicleService vehicleService;

	/**
	 * Handle the request of getting all the {@link MongoVehicle} from the
	 * Database.The {@link SqlUser} has to be logged in and has the role ADMIN
	 * 
	 * @return ResponseEntity<List<VehicleResponse>>
	 */
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "vehicle/all", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<VehicleResponse>> getAllVehicles() {
		List<Vehicle> allVehicles = new ArrayList<Vehicle>();
		List<VehicleResponse> allResponseVehicles = new ArrayList<VehicleResponse>();
		try {
			allVehicles = vehicleService.getAllVehiclesFromDb();
			for (Vehicle vehicle : allVehicles) {
				VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleResponseInstance(vehicle);
				allResponseVehicles.add(vehicleResponse);
			}
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleResponseErrorInstance(e.getMessage());
			allResponseVehicles.add(vehicleResponse);
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * Handle the request of creating a new {@link MongoVehicle} in the DataBase The
	 * {@link SqlUser} has to be logged in and has the role ADMIN
	 * 
	 * @param vehicleRequest
	 * @return ResponseEntity<List<VehicleResponse>>
	 */
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "vehicle/create/", method = RequestMethod.POST, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<VehicleResponse> insertVehicle(@RequestBody VehicleRequest vehicleRequest) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		try {
			Vehicle vehicle = VehicleRequestFactory.getVehicleInstanceFromVehicleRequest(vehicleRequest);
			vehicleService.insertVehicle(vehicle);
			vehicleResponse = VehicleResponseFactory.getVehicleResponseInstance(vehicle);
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			vehicleResponse = VehicleResponseFactory.getVehicleResponseErrorInstance(e.getMessage());
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
