package com.application.appProject.restController;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.mail.MessagingException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.application.appProject.SQLmodel.SqlContract;
import com.application.appProject.SQLmodel.SqlVehicle;
import com.application.appProject.factory.ContractRequestFactory;
import com.application.appProject.factory.ContractResponseFactory;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.restModel.ContractRequest;
import com.application.appProject.restModel.ContractResponse;
import com.application.appProject.service.ContractService;
import com.application.appProject.service.EmailService;

@RestController
@RequestMapping("/app2")
public class ContractSqlController {

	@Autowired
	@Qualifier("ContractSqlService")
	private ContractService contractService;

	@Autowired
	@Qualifier("EmailService")
	private EmailService email;

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contracts/all", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<SqlContract>> getAllContracts() {
		List<SqlContract> allVehicles = new ArrayList<SqlContract>();
		try {
			allVehicles = contractService.customGetAllContracts();
			return new ResponseEntity<List<SqlContract>>(allVehicles, HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<List<SqlContract>>(allVehicles, HttpStatus.BAD_REQUEST);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contracts/bestFive", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<SqlContract>> getFirstFiveVehicle() {
		List<SqlContract> allVehicles = new ArrayList<SqlContract>();
		try {
			allVehicles = contractService.getFirstFiveContractsOrderByNumber();
			return new ResponseEntity<List<SqlContract>>(allVehicles, HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<List<SqlContract>>(allVehicles, HttpStatus.BAD_REQUEST);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/create/", method = RequestMethod.POST, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<ContractResponse> insertNewContract(@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			ModelMapper modelMapper = new ModelMapper();
			SqlContract contract = modelMapper.map(contractRequest, SqlContract.class);
			contractService.insertNewContract(contract);
			contractResponse = ContractResponseFactory.getContractResponseInstance(contract);
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/activate/", method = RequestMethod.PUT, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<ContractResponse> activateContract(@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			ModelMapper modelMapper = new ModelMapper();
			SqlContract contract = modelMapper.map(contractRequest, SqlContract.class);
			contractService.activateExistingContract(contract);
			contractResponse = ContractResponseFactory.getContractResponseInstance(contract);
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/cancelActiveContract/", method = RequestMethod.PUT, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<ContractResponse> cancelActiveContract(@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			ModelMapper modelMapper = new ModelMapper();
			SqlContract contract = modelMapper.map(contractRequest, SqlContract.class);
			contractService.changeContractStateFromActiveToCanceled(contract);
			contractResponse = ContractResponseFactory.getContractResponseInstance(contract);
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/delete/", method = RequestMethod.DELETE, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<ContractResponse> deleteContract(@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			contractService.deleteContractByNumber(contractRequest.getNumber());
			contractResponse = ContractResponseFactory
					.getContractResponseSuccessInstance("Contract was deleted succesfull!");
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		}

	}

}
