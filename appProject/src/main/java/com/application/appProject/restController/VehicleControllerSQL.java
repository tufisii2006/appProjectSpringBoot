package com.application.appProject.restController;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.NoResultException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.web.bind.annotation.*;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.SQLmodel.SqlVehicle;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.factory.VehicleResponseFactory;
import com.application.appProject.restModel.VehicleRequest;
import com.application.appProject.restModel.VehicleResponse;
import com.application.appProject.service.VehicleService;

@RestController
@RequestMapping("/app2")
public class VehicleControllerSQL {

	@Autowired
	@Qualifier("VehicleServiceSql")
	private VehicleService vehicleService;

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "vehicle/one/{fin}", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<VehicleResponse> getSpecificVehicle(@PathVariable("fin") String fin) {
		try {
			Vehicle resultVehicle = vehicleService.getOneVehicleByFin(fin);
			VehicleResponse responseVehicles = VehicleResponseFactory.getVehicleResponseInstance(resultVehicle);
			return new ResponseEntity<VehicleResponse>(responseVehicles, HttpStatus.OK);
		} catch (NoResultException e) {
			VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleResponseErrorInstance(e.getMessage());
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.BAD_REQUEST);
		} catch (IllegalArgumentException e) {
			VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleResponseErrorInstance(e.getMessage());
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.BAD_REQUEST);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "vehicle/{brand}", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<SqlVehicle>> getAllVehicleWithGivenBrand(@PathVariable("brand") Brand brand) {
		List<SqlVehicle> allVehicles = new ArrayList<SqlVehicle>();
		try {
			allVehicles = vehicleService.getAllVehicleWithGivenBrand(brand);
			return new ResponseEntity<List<SqlVehicle>>(allVehicles, HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<List<SqlVehicle>>(allVehicles, HttpStatus.BAD_REQUEST);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "vehicle/all", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<Vehicle>> getAllVehicles() {
		List<Vehicle> allVehicles = new ArrayList<Vehicle>();
		try {
			allVehicles = vehicleService.getAllVehiclesFromDb();
			return new ResponseEntity<List<Vehicle>>(allVehicles, HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<List<Vehicle>>(allVehicles, HttpStatus.BAD_REQUEST);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "vehicle/insert", method = RequestMethod.POST, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<Vehicle> insertVehicle(@RequestBody VehicleRequest vehicleRequest) {
		Vehicle vehicle = new Vehicle();
		try {
			ModelMapper modelMapper = new ModelMapper();
			vehicle = modelMapper.map(vehicleRequest, SqlVehicle.class);
			vehicleService.insertVehicle(vehicle);
			return new ResponseEntity<Vehicle>(vehicle, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			return new ResponseEntity<Vehicle>(vehicle, HttpStatus.BAD_REQUEST);
		}

	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "vehicle/delete", method = RequestMethod.DELETE, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<Vehicle> deleteVehicleById(@RequestBody VehicleRequest vehicleRequest) {
		Vehicle vehicle = new Vehicle();
		try {
			vehicleService.deleteVehicleFromDb(vehicleRequest.getId());
			return new ResponseEntity<Vehicle>(vehicle, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			return new ResponseEntity<Vehicle>(vehicle, HttpStatus.I_AM_A_TEAPOT);
		}

	}

}
