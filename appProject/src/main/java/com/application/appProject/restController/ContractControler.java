package com.application.appProject.restController;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.application.appProject.factory.ContractRequestFactory;
import com.application.appProject.factory.ContractResponseFactory;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.mongoRepository.UserMongoRepository;
import com.application.appProject.restModel.ContractRequest;
import com.application.appProject.restModel.ContractResponse;
import com.application.appProject.service.*;

@RestController
@RequestMapping("/app1")
public class ContractControler {
	@Autowired
	@Qualifier("ContractMongoService")
	private ContractService contractService;

	/**
	 * Creates a new {@link MongoContract} in the DataBase.The {@link MongoUser} has
	 * to be logged in and have the role ADMIN
	 * 
	 * @param contractRequest
	 * @return ResponseEntity<ContractResponse>
	 */
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/create/", method = RequestMethod.POST, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<ContractResponse> insertNewContract(@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			MongoContract contract = ContractRequestFactory.getContractInstanceFromContractRequest(contractRequest);
			contractService.insertNewContract(contract);
			contractResponse = ContractResponseFactory.getContractResponseInstance(contract);
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.NOT_ACCEPTABLE);
		}

	}

	/**
	 * Activate an existing {@link MongoContract} .The {@link MongoUser} has to be
	 * logged in and have the role ADMIN.Only a contract having the state
	 * IN_PREPARATION can be activated
	 * 
	 * @param contractRequest
	 * @return ResponseEntity<ContractResponse>
	 */

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/activate/", method = RequestMethod.PATCH, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<ContractResponse> activateExistingContract(@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			MongoContract contract = ContractRequestFactory.getContractInstanceFromContractRequest(contractRequest);
			contractService.activateExistingContract(contract);
			contractResponse = ContractResponseFactory.getContractResponseInstance(contract);
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Cancel an existing {@link MongoContract} .The {@link MongoUser} has to be
	 * logged in and have the role ADMIN.The {@link MongoContract} having the state
	 * IN_PREPARATION can be canceled
	 * 
	 * @param contractRequest
	 * @return ResponseEntity<ContractResponse>
	 */

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/cancelPreparedContract/", method = RequestMethod.PATCH, consumes = {
			"application/xml", "application/json" })
	public ResponseEntity<ContractResponse> cancelInPreparationContract(@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			MongoContract contract = ContractRequestFactory.getContractInstanceFromContractRequest(contractRequest);
			contractService.changeContractStateFromIn_PreparationToCanceled(contract);
			contractResponse = ContractResponseFactory.getContractResponseInstance(contract);
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Cancel an existing {@link MongoContract} .The {@link MongoUser} has to be
	 * logged in and have the role ADMIN.The {@link MongoContract} having the state
	 * ACTIVE can be canceled
	 * 
	 * @param contractRequest
	 * @return ResponseEntity<ContractResponse>
	 */
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(path = "contract/cancelActiveContract/", method = RequestMethod.PATCH, consumes = {
			"application/xml", "application/json" })
	public ResponseEntity<ContractResponse> changeContractStateFromActiveToCanceled(
			@RequestBody ContractRequest contractRequest) {
		ContractResponse contractResponse = new ContractResponse();
		try {
			MongoContract contract = ContractRequestFactory.getContractInstanceFromContractRequest(contractRequest);
			contractService.changeContractStateFromActiveToCanceled(contract);
			contractResponse = ContractResponseFactory.getContractResponseInstance(contract);
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IllegalArgumentException e) {
			contractResponse = ContractResponseFactory.getContractResponseErrorInstance(e.getMessage());
			return new ResponseEntity<ContractResponse>(contractResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
