package com.application.appProject.mongoModel;

import java.util.Date;

import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.application.appProject.abstractModel.Branch;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Creator;
import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.service.VehicleServiceMongoImpl;

@Document(collection = "vehicle")
public class MongoVehicle extends Vehicle {

	@Id
	@NotNull
	private String id;

	@NotNull
	private String fin;
	@NotNull
	private Branch branch;
	private String modelYear;
	@NotNull
	private Brand brand;
	@NotNull
	private String vehicleClass;
	private String model;
	private String transmissionType;
	private String engineType;
	private String commisionNumber;
	private Date registrationDate;
	private String emissionStandard;
	private Creator createdBy;
	private Date createdByTimeStamp;
	private Date modifiedByTimeStamp;
	private Creator modifiedBy;

	public MongoVehicle() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getVehicleClass() {
		return vehicleClass;
	}

	public void setVehicleClass(String vehicleClass) {
		this.vehicleClass = vehicleClass;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getTransmissionType() {
		return transmissionType;
	}

	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getCommisionNumber() {
		return commisionNumber;
	}

	public void setCommisionNumber(String commisionNumber) {
		this.commisionNumber = commisionNumber;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getEmissionStandard() {
		return emissionStandard;
	}

	public void setEmissionStandard(String emissionStandard) {
		this.emissionStandard = emissionStandard;
	}

	public Creator getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Creator createdBy) {
		this.createdBy = createdBy;
	}

	public Creator getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Creator modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreatedByTimeStamp() {
		return createdByTimeStamp;
	}

	public void setCreatedByTimeStamp(Date createdByTimeStamp) {
		this.createdByTimeStamp = createdByTimeStamp;
	}

	public Date getModifiedByTimeStamp() {
		return modifiedByTimeStamp;
	}

	public void setModifiedByTimeStamp(Date modifiedByTimeStamp) {
		this.modifiedByTimeStamp = modifiedByTimeStamp;
	}

	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", fin=" + fin + ", branch=" + branch + ", modelYear=" + modelYear + ", brand="
				+ brand + ", vehicleClass=" + vehicleClass + ", model=" + model + ", transmissionType="
				+ transmissionType + ", engineType=" + engineType + ", commisionNumber=" + commisionNumber
				+ ", registrationDate=" + registrationDate + ", emissionStandard=" + emissionStandard + ", createdBy="
				+ createdBy + ", createdByTimeStamp=" + createdByTimeStamp + ", modifiedByTimeStamp="
				+ modifiedByTimeStamp + ", modifiedBy=" + modifiedBy + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branch == null) ? 0 : branch.hashCode());
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + ((commisionNumber == null) ? 0 : commisionNumber.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((createdByTimeStamp == null) ? 0 : createdByTimeStamp.hashCode());
		result = prime * result + ((emissionStandard == null) ? 0 : emissionStandard.hashCode());
		result = prime * result + ((engineType == null) ? 0 : engineType.hashCode());
		result = prime * result + ((fin == null) ? 0 : fin.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((modelYear == null) ? 0 : modelYear.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		result = prime * result + ((modifiedByTimeStamp == null) ? 0 : modifiedByTimeStamp.hashCode());
		result = prime * result + ((registrationDate == null) ? 0 : registrationDate.hashCode());
		result = prime * result + ((transmissionType == null) ? 0 : transmissionType.hashCode());
		result = prime * result + ((vehicleClass == null) ? 0 : vehicleClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MongoVehicle other = (MongoVehicle) obj;
		if (branch != other.branch)
			return false;
		if (brand != other.brand)
			return false;
		if (commisionNumber == null) {
			if (other.commisionNumber != null)
				return false;
		} else if (!commisionNumber.equals(other.commisionNumber))
			return false;
		if (createdBy != other.createdBy)
			return false;
		if (createdByTimeStamp == null) {
			if (other.createdByTimeStamp != null)
				return false;
		} else if (!createdByTimeStamp.equals(other.createdByTimeStamp))
			return false;
		if (emissionStandard == null) {
			if (other.emissionStandard != null)
				return false;
		} else if (!emissionStandard.equals(other.emissionStandard))
			return false;
		if (engineType == null) {
			if (other.engineType != null)
				return false;
		} else if (!engineType.equals(other.engineType))
			return false;
		if (fin == null) {
			if (other.fin != null)
				return false;
		} else if (!fin.equals(other.fin))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (modelYear == null) {
			if (other.modelYear != null)
				return false;
		} else if (!modelYear.equals(other.modelYear))
			return false;
		if (modifiedBy != other.modifiedBy)
			return false;
		if (modifiedByTimeStamp == null) {
			if (other.modifiedByTimeStamp != null)
				return false;
		} else if (!modifiedByTimeStamp.equals(other.modifiedByTimeStamp))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (transmissionType == null) {
			if (other.transmissionType != null)
				return false;
		} else if (!transmissionType.equals(other.transmissionType))
			return false;
		if (vehicleClass == null) {
			if (other.vehicleClass != null)
				return false;
		} else if (!vehicleClass.equals(other.vehicleClass))
			return false;
		return true;
	}

}
