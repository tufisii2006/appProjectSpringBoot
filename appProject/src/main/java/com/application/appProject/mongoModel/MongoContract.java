package com.application.appProject.mongoModel;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.application.appProject.abstractModel.Contract;
import com.application.appProject.abstractModel.Portofolio;
import com.application.appProject.abstractModel.State;

@Document(collection = "contract")
public class MongoContract extends Contract {
	@Id
	//@NotNull
	private String id;
	private String number;
	//@NotNull
	private State state;
	//@NotNull
	private String vehicleFin; 
	private Date startDate;
	private Integer duration;
	//@NotNull
	private Portofolio portofolio;
	private BigDecimal rvg;
	private BigDecimal rateSubvention;
	private Integer mileagePerYear;
	private String ruGuarantor;
	// @NotNull
	private Date createdTimeStamp;

	private Date modifiedTimeStamp;
	private Integer contractVersion;
	private Date cancelationDate;
	private String createdByUserCnp;
	private String modifiedByUserCnp;

	public MongoContract() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Portofolio getPortofolio() {
		return portofolio;
	}

	public void setPortofolio(Portofolio portofolio) {
		this.portofolio = portofolio;
	}

	public BigDecimal getRvg() {
		return rvg;
	}

	public void setRvg(BigDecimal rvg) {
		this.rvg = rvg;
	}

	public BigDecimal getRateSubvention() {
		return rateSubvention;
	}

	public void setRateSubvention(BigDecimal rateSubvention) {
		this.rateSubvention = rateSubvention;
	}

	public Integer getMileagePerYear() {
		return mileagePerYear;
	}

	public void setMileagePerYear(Integer mileagePerYear) {
		this.mileagePerYear = mileagePerYear;
	}

	public String getRuGuarantor() {
		return ruGuarantor;
	}

	public void setRuGuarantor(String ruGuarantor) {
		this.ruGuarantor = ruGuarantor;
	}


	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public String getCreatedByUserCnp() {
		return createdByUserCnp;
	}

	public void setCreatedByUserCnp(String createdByUserCnp) {
		this.createdByUserCnp = createdByUserCnp;
	}

	public String getModifiedByUserCnp() {
		return modifiedByUserCnp;
	}

	public void setModifiedByUserCnp(String modifiedByUserCnp) {
		this.modifiedByUserCnp = modifiedByUserCnp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Date getModifiedTimeStamp() {
		return modifiedTimeStamp;
	}

	public void setModifiedTimeStamp(Date modifiedTimeStamp) {
		this.modifiedTimeStamp = modifiedTimeStamp;
	}

	public Integer getContractVersion() {
		return contractVersion;
	}

	public void setContractVersion(Integer contractVersion) {
		this.contractVersion = contractVersion;
	}

	public Date getCancelationDate() {
		return cancelationDate;
	}

	public void setCancelationDate(Date cancelationDate) {
		this.cancelationDate = cancelationDate;
	}

	public String getVehicleFin() {
		return vehicleFin;
	}

	public void setVehicleFin(String vehicleFin) {
		this.vehicleFin = vehicleFin;
	}

}
