package com.application.appProject.mappers;

import org.modelmapper.ModelMapper;

import com.application.appProject.abstractModel.User;
import com.application.appProject.mongoModel.*;
import com.application.appProject.restModel.*;

/**
 * Provide mapping for User entity
 * 
 * @author radu.tufisi
 *
 */
public class UserMapper {

	/**
	 * Compute the mapping from {@link UserRequest} to {@link MongoUser}
	 * 
	 * @param userRequest
	 * @return {@link MongoUser}
	 */
	public static User mapRequestToEntity(UserRequest userRequest) {
		ModelMapper modelMapper = new ModelMapper();
		User user = modelMapper.map(userRequest, User.class);
		return user;
	}
	
	public static User mapRequestToSql(UserRequest userRequest) {
		ModelMapper modelMapper = new ModelMapper();
		User user = modelMapper.map(userRequest, User.class);
		return user;
	}

	/**
	 * Compute the mapping from {@link MongoUser} to {@link UserResponse}
	 * 
	 * @param user
	 * @return {@link UserReponse}
	 */
	public static UserResponse mapEntityToResponse(User user) {
		ModelMapper modelMapper = new ModelMapper();
		UserResponse userResponse = modelMapper.map(user, UserResponse.class);
		return userResponse;
	}

	/**
	 * Compute the mapping from {@link UserRequestUpdatePassword} to
	 * {@link MongoUser}
	 * 
	 * @param userRequest
	 * @return {@link MongoUser}
	 */
	public static User mapUserPasswordRequestToEntity(UserRequestUpdatePassword userRequest) {
		ModelMapper modelMapper = new ModelMapper();
		User user = modelMapper.map(userRequest, User.class);
		return user;
	}

}
