package com.application.appProject.mappers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.application.appProject.abstractModel.Contract;
import com.application.appProject.mongoModel.*;
import com.application.appProject.mongoRepository.UserMongoRepository;
import com.application.appProject.restModel.*;

/**
 * Provide mapping for Contract entity
 * 
 * @author radu.tufisi
 *
 */
public class ContractMapper {

	/**
	 * Compute the mapping from {@link ContractRequest} to {@link MongoContract}
	 * 
	 * @param contractRequest
	 * @return {@link MongoContract}
	 */
	public static MongoContract mapRequestToEntity(ContractRequest contractRequest) {
		ModelMapper modelMapper = new ModelMapper();
		MongoContract contract = modelMapper.map(contractRequest, MongoContract.class);	
		return contract;
	}

	/**
	 * Compute the mapping from {@link MongoContract} to {@link ContractRequest}
	 * 
	 * @param contract
	 * @return {@link ContractRequest}
	 */
	public static ContractResponse mapEntityToResponse(Contract contract) {
		ModelMapper modelMapper = new ModelMapper();
		ContractResponse contractResponse = modelMapper.map(contract, ContractResponse.class);
		return contractResponse;
	}
}
