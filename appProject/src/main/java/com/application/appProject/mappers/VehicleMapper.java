package com.application.appProject.mappers;

import org.modelmapper.ModelMapper;

import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.restModel.VehicleRequest;
import com.application.appProject.restModel.VehicleResponse;

/**
 * Provide mapping for Vehicle entity
 * 
 * @author radu.tufisi
 *
 */
public class VehicleMapper {

	/**
	 * Compute the mapping from {@link VehicleRequest} to {@link MongoVehicle}
	 * 
	 * @param vehicleRequest
	 * @return {@link MongoVehicle}
	 */
	public static Vehicle mapRequestToEntity(VehicleRequest vehicleRequest) {
		ModelMapper modelMapper = new ModelMapper();
		Vehicle vehicle = modelMapper.map(vehicleRequest, Vehicle.class);
		return vehicle;
	}

	/**
	 * Compute the mapping from {@link MongoVehicle} to {@link VehicleResponse}
	 * 
	 * @param vehicle
	 * @return {@link VehicleResponse}
	 */
	public static VehicleResponse mapEntityToResponse(Vehicle vehicle) {
		ModelMapper modelMapper = new ModelMapper();
		VehicleResponse vehicleResponse = modelMapper.map(vehicle, VehicleResponse.class);
		return vehicleResponse;
	}

}
