package com.application.appProject.validators;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.StringUtils;

import com.application.appProject.abstractModel.State;
import com.application.appProject.mongoModel.MongoContract;

public class ContractValidator {

	/**
	 * Checks if the contract rvg is valid
	 * 
	 * @param rvg
	 * @return true if the contract rvg has only 2 digits after digit(x.xx) or false
	 *         otherwise
	 */
	public static boolean isContractRvgValid(double rvg) {
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		double testRvg = Double.valueOf(decimalFormat.format(rvg));
		if (testRvg == rvg) {
			return true;
		}

		return false;

	}

	/**
	 * Checks if the {@link MongoContract} is valid to be canceled when {@link State} is
	 * ACTIVE
	 * 
	 * @param contractStartDate
	 * @return true - If the {@link MongoContract} start date is at least one year before
	 *         the current date or false otherwise
	 */
	public static boolean isContractValidToBeCanceledWhenItIsActive(Date contractStartDate) {
		if (contractStartDate == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(contractStartDate);
		calendar.add(Calendar.YEAR, 1);
		Date validEndingDate = calendar.getTime();
		Date currentDate = new Date();
		if (validEndingDate.before(currentDate)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param contract
	 * @return true if {@link MongoContract} {@link State} and number are valid or false
	 *         otherwise
	 * @throws IllegalArgumentException
	 *             if the {@link MongoContract} is null , its number is null or
	 *             {@link State} is not IN_PREPARATION
	 */
	public static boolean isContractValidToChangeStateFromIn_PreparationToCanceled(MongoContract contract)
			throws IllegalArgumentException {
		if (contract == null) {
			throw new IllegalArgumentException("The contract is null!");
		}
		if (StringUtils.isEmpty(contract.getNumber())) {
			throw new IllegalArgumentException("The contract number is null or invalid!");
		}
		if (contract.getState() != State.IN_PREPARATION) {
			throw new IllegalArgumentException("The contract must have IN_PREPARATION state!");
		}
		return true;
	}

	/**
	 * Performs the validation of the contract.
	 * 
	 * @param contract
	 * @return true if the contract is valid or false otherwise
	 */

	public static boolean isContractValid(MongoContract contract) {
		if (contract == null) {
			return false;
		}
		if (contract.getMileagePerYear() == null || contract.getRvg() == null
				|| StringUtils.isEmpty(contract.getRuGuarantor()) || contract.getState() == null
				|| StringUtils.isEmpty(contract.getNumber()) || contract.getRateSubvention() == null
				|| StringUtils.isEmpty(contract.getVehicleFin())) {
			return false;
		}
		if (contract.getNumber().length() != ValidatorConstants.contractNumberLength
				|| contract.getRuGuarantor().length() != ValidatorConstants.contractRuGuarantorLength) {
			return false;
		}

		if (!isContractRvgValid(contract.getRvg().doubleValue())) {
			return false;
		}
		return true;
	}

}
