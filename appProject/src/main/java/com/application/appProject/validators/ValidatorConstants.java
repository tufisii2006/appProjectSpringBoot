package com.application.appProject.validators;

public class ValidatorConstants {

	public static final String emailRegex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	public static final String cnpRegex = "\\d{13}";
	public static final String passwordRegex = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#./$%]).{6,20})";
	public static final String finRegex = "\\d{3}\\w{14}";

	public static final int vehicleFinLength = 17;
	public static final int vehicleIdLength = 21;
	public static final int vehicleModelYearLength = 3;
	public static final int vehicleTransmissionTypeLength = 10;
	public static final int vehicleEngineTypeLength = 10;
	public static final int vehicleCommisionNumberLength = 14;
	public static final int vehicleEmissionStandardLength = 10;

	public static final int contractRuGuarantorLength = 10;
	public static final int contractNumberLength = 10;
}
