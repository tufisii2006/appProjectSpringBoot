package com.application.appProject.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.application.appProject.abstractModel.User;
import com.application.appProject.mongoModel.*;

public class UserValidator {

	/**
	 * Checks if the input String parameter has a email pattern. Method should
	 * return true for a String having pattern email@server.com
	 * 
	 * @param emailString
	 * @return true if the email address pattern is valid or false otherwise
	 */
	public static boolean isUserEmailAddressValid(String emailString) {
		Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(ValidatorConstants.emailRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailString);
		return matcher.find();
	}

	/**
	 * Checks if the input String parameter has a CNP pattern. Method should return
	 * true for a String that has 13 digits
	 * 
	 * @param cnp
	 * @return true if the cnp pattern is valid or false otherwise
	 */
	public static boolean isUserCnpValid(String cnp) {
		Pattern VALID_CNP_REGEX = Pattern.compile(ValidatorConstants.cnpRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = VALID_CNP_REGEX.matcher(cnp);
		return matcher.find();
	}

	/**
	 * Checks if the input String parameter has a strong password pattern. Method
	 * should return true for a String that must contains one digit from 0-9,one
	 * lower case characters,one upper case characters, one special symbols in the
	 * list "@#$%",length at least 6 characters and maximum of 20
	 * 
	 * @param password
	 * @return true if the password pattern is valid or false otherwise
	 */
	public static boolean isUserPasswordValid(String password) {
		Pattern VALID_PASSWORD_REGEX = Pattern.compile(ValidatorConstants.passwordRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = VALID_PASSWORD_REGEX.matcher(password);
		return matcher.find();
	}

	/**
	 * Checks if the User given as parameter is a valid one.Performs validation of
	 * User(including null) and the fields.
	 * 
	 * @param user
	 * @return true if the User is validated successfully or false otherwise
	 */

	public static boolean isUserValid(User user) {
		if (user == null) {
			return false;
		}
		if (StringUtils.isEmpty(user.getCnp()) || StringUtils.isEmpty(user.getEmail())
				|| StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword())
				|| StringUtils.isEmpty(user.getFirstName()) || StringUtils.isEmpty(user.getLastName())) {
			return false;
		}

		if (isUserCnpValid(user.getCnp()) == false || isUserPasswordValid(user.getPassword()) == false
				|| isUserEmailAddressValid(user.getEmail()) == false) {
			return false;
		}
		return true;
	}

}
