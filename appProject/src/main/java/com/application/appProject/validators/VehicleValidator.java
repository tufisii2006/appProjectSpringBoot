package com.application.appProject.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

import com.application.appProject.abstractModel.Branch;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.mongoModel.MongoVehicle;

public class VehicleValidator {

	/**
	 * Check if the input String has a certain length
	 * 
	 * @param inputValue
	 * @param validLength
	 * @return true if the first parameter length is equal with second parameter
	 */
	public static boolean isInputStringLengthValid(String inputValue, int validLength) {
		if (inputValue.length() != validLength) {
			return false;
		}
		return true;
	}

	/**
	 * Check if the vehicle ID is valid
	 * 
	 * @param id
	 * @param fin
	 * @param branch
	 * @param brand
	 * @return true if the ID of vehicle is the concatenation of vehicle fin,branch
	 *         and brand(in this order) or false otherwise
	 */
	public static boolean isVehicleIdValid(String id, String fin, Branch branch, Brand brand) {
		if (!StringUtils.contains(id, branch.value()) || !StringUtils.contains(id, fin)
				|| !StringUtils.contains(id, brand.value())) {
			return false;
		}
		if (!StringUtils.startsWith(id, fin) && !StringUtils.endsWith(id, brand.value())) {
			return false;
		}
		String testId = fin + branch.value() + brand.value();
		if (testId.equals(id) == false) {
			return false;
		}
		return true;
	}

	/**
	 * Check if the vehicle fin is valid
	 * 
	 * @param fin
	 * @return true if the fin pattern is valid(starts with 3 digits followed by 14
	 *         letters) or false otherwise
	 */
	public static boolean isVehicleFinValid(String fin) {
		Pattern VALID_FIN_REGEX = Pattern.compile(ValidatorConstants.finRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = VALID_FIN_REGEX.matcher(fin);
		return matcher.find();
	}

	/**
	 * Check if the input Vehicle is valid
	 * 
	 * @param vehicle
	 * @return true if all Vehicle constraints are satisfied or false otherwise
	 */
	public static boolean isVehicleValid(Vehicle vehicle) {
		if (vehicle == null) {
			return false;
		}
		if (StringUtils.isEmpty(vehicle.getFin()) || vehicle.getBranch() == null || vehicle.getBrand() == null
				|| StringUtils.isEmpty(vehicle.getVehicleClass()) || StringUtils.isEmpty(vehicle.getId())
				|| vehicle.getCreatedBy() == null) {
			return false;
		}
		if (!isInputStringLengthValid(vehicle.getFin(), ValidatorConstants.vehicleFinLength)
				|| !isInputStringLengthValid(vehicle.getId(), ValidatorConstants.vehicleIdLength)
				|| !isInputStringLengthValid(vehicle.getModelYear(), ValidatorConstants.vehicleModelYearLength)
				|| !isInputStringLengthValid(vehicle.getTransmissionType(),
						ValidatorConstants.vehicleTransmissionTypeLength)
				|| !isInputStringLengthValid(vehicle.getEngineType(), ValidatorConstants.vehicleEngineTypeLength)
				|| !isInputStringLengthValid(vehicle.getCommisionNumber(),
						ValidatorConstants.vehicleCommisionNumberLength)
				|| !isInputStringLengthValid(vehicle.getEmissionStandard(),
						ValidatorConstants.vehicleEmissionStandardLength)) {
			return false;
		}
		if (!isVehicleFinValid(vehicle.getFin())) {
			return false;
		}
		if (!isVehicleIdValid(vehicle.getId(), vehicle.getFin(), vehicle.getBranch(), vehicle.getBrand())) {
			return false;
		}
		return true;
	}

}
