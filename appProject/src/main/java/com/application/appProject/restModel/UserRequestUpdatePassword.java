package com.application.appProject.restModel;

public class UserRequestUpdatePassword {
	private String cnp;
	private String password;

	public UserRequestUpdatePassword() {

	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
