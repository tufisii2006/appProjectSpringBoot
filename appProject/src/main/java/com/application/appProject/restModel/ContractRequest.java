package com.application.appProject.restModel;

import java.math.BigDecimal;
import java.util.Date;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.abstractModel.Portofolio;

public class ContractRequest {

	private String number;

	private String vehicleFin;

	private Integer duration;
	private Portofolio portofolio;
	private BigDecimal rvg;
	private BigDecimal rateSubvention;
	private Integer mileagePerYear;
	private String ruGuarantor;

	private String createdByUserCnp;
	private String modifiedByUserCnp;
	SqlUser user;

	public ContractRequest() {

	}

	public String getCreatedByUserCnp() {
		return createdByUserCnp;
	}

	public void setCreatedByUserCnp(String createdByUserCnp) {
		this.createdByUserCnp = createdByUserCnp;
	}

	public String getModifiedByUserCnp() {
		return modifiedByUserCnp;
	}

	public void setModifiedByUserCnp(String modifiedByUserCnp) {
		this.modifiedByUserCnp = modifiedByUserCnp;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Portofolio getPortofolio() {
		return portofolio;
	}

	public void setPortofolio(Portofolio portofolio) {
		this.portofolio = portofolio;
	}

	public BigDecimal getRvg() {
		return rvg;
	}

	public void setRvg(BigDecimal rvg) {
		this.rvg = rvg;
	}

	public BigDecimal getRateSubvention() {
		return rateSubvention;
	}

	public void setRateSubvention(BigDecimal rateSubvention) {
		this.rateSubvention = rateSubvention;
	}

	public Integer getMileagePerYear() {
		return mileagePerYear;
	}

	public void setMileagePerYear(Integer mileagePerYear) {
		this.mileagePerYear = mileagePerYear;
	}

	public String getRuGuarantor() {
		return ruGuarantor;
	}

	public void setRuGuarantor(String ruGuarantor) {
		this.ruGuarantor = ruGuarantor;
	}

	public String getVehicleFin() {
		return vehicleFin;
	}

	public void setVehicleFin(String vehicleFin) {
		this.vehicleFin = vehicleFin;
	}

}
