package com.application.appProject.restModel;

import java.util.Date;

import com.application.appProject.abstractModel.Branch;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Creator;

public class VehicleResponse {

	private String id;
	private String vehicleFin;
	private Branch branch;
	private String modelYear;
	private Brand brand;
	private String vehicleClass;
	private String model;
	private String transmissionType;
	private String engineType;
	private String commisionNumber;
	private Date registrationDate;
	private String emissionStandard;
	private Creator createdBy;
	private Date createdByTimeStamp;
	private Date modifiedByTimeStamp;
	private Creator modifiedBy;
	
	private String responseMessage;

	public VehicleResponse() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getVehicleClass() {
		return vehicleClass;
	}

	public void setVehicleClass(String vehicleClass) {
		this.vehicleClass = vehicleClass;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getTransmissionType() {
		return transmissionType;
	}

	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getCommisionNumber() {
		return commisionNumber;
	}

	public void setCommisionNumber(String commisionNumber) {
		this.commisionNumber = commisionNumber;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getEmissionStandard() {
		return emissionStandard;
	}

	public void setEmissionStandard(String emissionStandard) {
		this.emissionStandard = emissionStandard;
	}

	public Creator getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Creator createdBy) {
		this.createdBy = createdBy;
	}

	public Creator getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Creator modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreatedByTimeStamp() {
		return createdByTimeStamp;
	}

	public void setCreatedByTimeStamp(Date createdByTimeStamp) {
		this.createdByTimeStamp = createdByTimeStamp;
	}

	public Date getModifiedByTimeStamp() {
		return modifiedByTimeStamp;
	}

	public void setModifiedByTimeStamp(Date modifiedByTimeStamp) {
		this.modifiedByTimeStamp = modifiedByTimeStamp;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getVehicleFin() {
		return vehicleFin;
	}

	public void setVehicleFin(String vehicleFin) {
		this.vehicleFin = vehicleFin;
	}

}
