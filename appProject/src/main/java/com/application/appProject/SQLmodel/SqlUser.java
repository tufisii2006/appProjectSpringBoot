package com.application.appProject.SQLmodel;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import com.application.appProject.abstractModel.User;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "users")
public class SqlUser extends User implements UserDetails {

	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@NotBlank
	private String cnp;

	@NotBlank(message = "Name cannot be null")
	@Size(min = 3, max = 20, message = "Invalid length of first name")
	@Column(name = "fname")
	private String firstName;

	// @NotBlank
	// @Size(min = 3, max = 20, message = "Adfsfadfa")
	@Column(name = "lname")
	private String lastName;

	// @NotBlank
	// @Email(message = " bad email")
	@Column(name = "usermail")
	private String email;

	// @NotBlank
	// @Size(max = 10)
	@Column(name = "userPhone")
	private String phone;

	// @NotBlank
	@Column
	private String role;

	// @NotBlank
	@Column
	private String password;

	// @NotBlank
	@Column
	private String username;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		/*
		 * return AuthorityUtils.createAuthorityList(Role.ADMIN.value(),
		 * Role.USER.value(), Role.EXPLORER.value());
		 */
		return AuthorityUtils.createAuthorityList(this.getRole());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
