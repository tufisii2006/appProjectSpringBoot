package com.application.appProject.SQLmodel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.application.appProject.abstractModel.Branch;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Creator;
import com.application.appProject.abstractModel.Vehicle;

@Entity
@Table(name = "vehicle")
@EntityListeners(AuditingEntityListener.class)
@NamedQueries({
		@NamedQuery(name = "SqlVehicle.getAllVehicleOfGivenBrand", query = "select v from SqlVehicle v where v.brand=:brand "),
		@NamedQuery(name = "SqlVehicle.findThatSpecificCar", query = "select v from SqlVehicle v where v.fin=:fin ") })
public class SqlVehicle extends Vehicle {

	@Id
	@NotBlank
	private String id;
	@Column
	private String fin;
	@Column
	@Enumerated(EnumType.STRING)
	private Branch branch;
	@Column
	private String modelYear;
	@Column
	@Enumerated(EnumType.STRING)
	private Brand brand;
	@Column
	private String vehicleClass;
	@Column
	private String model;
	@Column
	private String transmissionType;
	@Column
	private String engineType;
	@Column
	private String commisionNumber;
	@Column
	private Date registrationDate;
	@Column
	private String emissionStandard;
	@Column
	@Enumerated(EnumType.STRING)
	private Creator createdBy;
	@Column
	private Date createdByTimeStamp;
	@Column
	private Date modifiedByTimeStamp;
	@Column
	@Enumerated(EnumType.STRING)
	private Creator modifiedBy;

	public SqlVehicle() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getVehicleClass() {
		return vehicleClass;
	}

	public void setVehicleClass(String vehicleClass) {
		this.vehicleClass = vehicleClass;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getTransmissionType() {
		return transmissionType;
	}

	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getCommisionNumber() {
		return commisionNumber;
	}

	public void setCommisionNumber(String commisionNumber) {
		this.commisionNumber = commisionNumber;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getEmissionStandard() {
		return emissionStandard;
	}

	public void setEmissionStandard(String emissionStandard) {
		this.emissionStandard = emissionStandard;
	}

	public Creator getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Creator createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedByTimeStamp() {
		return createdByTimeStamp;
	}

	public void setCreatedByTimeStamp(Date createdByTimeStamp) {
		this.createdByTimeStamp = createdByTimeStamp;
	}

	public Date getModifiedByTimeStamp() {
		return modifiedByTimeStamp;
	}

	public void setModifiedByTimeStamp(Date modifiedByTimeStamp) {
		this.modifiedByTimeStamp = modifiedByTimeStamp;
	}

	public Creator getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Creator modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", fin=" + fin + ", branch=" + branch + ", modelYear=" + modelYear + ", brand="
				+ brand + ", vehicleClass=" + vehicleClass + ", model=" + model + ", transmissionType="
				+ transmissionType + ", engineType=" + engineType + ", commisionNumber=" + commisionNumber
				+ ", registrationDate=" + registrationDate + ", emissionStandard=" + emissionStandard + ", createdBy="
				+ createdBy + ", createdByTimeStamp=" + createdByTimeStamp + ", modifiedByTimeStamp="
				+ modifiedByTimeStamp + ", modifiedBy=" + modifiedBy + "]";
	}

}
