package com.application.appProject.SQLmodel;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.application.appProject.abstractModel.Contract;
import com.application.appProject.abstractModel.Portofolio;
import com.application.appProject.abstractModel.State;

@Entity
@Table(name = "contracts")
@EntityListeners(AuditingEntityListener.class)
public class SqlContract extends Contract {

	@Id
	@NotBlank
	@Column
	private String id;

	@Column
	private String number;
	@Column

	private State state;
	@Column
	private String vehicleFin;
	@Column
	private Date startDate;
	@Column
	private Integer duration;
	@Column

	private Portofolio portofolio;
	@Column
	private BigDecimal rvg;
	@Column
	private BigDecimal rateSubvention;
	@Column
	private Integer mileagePerYear;
	@Column
	private String ruGuarantor;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdTimeStamp;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date modifiedTimeStamp;

	@Column
	private Integer contractVersion;
	@Column
	private Date cancelationDate;
	@Column
	private String createdByUserCnp;
	@Column
	private String modifiedByUserCnp;

	@Column
	@CreatedBy
	SqlUser user;

	public SqlContract() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Portofolio getPortofolio() {
		return portofolio;
	}

	public void setPortofolio(Portofolio portofolio) {
		this.portofolio = portofolio;
	}

	public BigDecimal getRvg() {
		return rvg;
	}

	public void setRvg(BigDecimal rvg) {
		this.rvg = rvg;
	}

	public BigDecimal getRateSubvention() {
		return rateSubvention;
	}

	public void setRateSubvention(BigDecimal rateSubvention) {
		this.rateSubvention = rateSubvention;
	}

	public Integer getMileagePerYear() {
		return mileagePerYear;
	}

	public void setMileagePerYear(Integer mileagePerYear) {
		this.mileagePerYear = mileagePerYear;
	}

	public String getRuGuarantor() {
		return ruGuarantor;
	}

	public void setRuGuarantor(String ruGuarantor) {
		this.ruGuarantor = ruGuarantor;
	}

	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public String getCreatedByUserCnp() {
		return createdByUserCnp;
	}

	public void setCreatedByUserCnp(String createdByUserCnp) {
		this.createdByUserCnp = createdByUserCnp;
	}

	public String getModifiedByUserCnp() {
		return modifiedByUserCnp;
	}

	public void setModifiedByUserCnp(String modifiedByUserCnp) {
		this.modifiedByUserCnp = modifiedByUserCnp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Date getModifiedTimeStamp() {
		return modifiedTimeStamp;
	}

	public void setModifiedTimeStamp(Date modifiedTimeStamp) {
		this.modifiedTimeStamp = modifiedTimeStamp;
	}

	public Integer getContractVersion() {
		return contractVersion;
	}

	public void setContractVersion(Integer contractVersion) {
		this.contractVersion = contractVersion;
	}

	public Date getCancelationDate() {
		return cancelationDate;
	}

	public void setCancelationDate(Date cancelationDate) {
		this.cancelationDate = cancelationDate;
	}

	public String getVehicleFin() {
		return vehicleFin;
	}

	public void setVehicleFin(String vehicleFin) {
		this.vehicleFin = vehicleFin;
	}

}
