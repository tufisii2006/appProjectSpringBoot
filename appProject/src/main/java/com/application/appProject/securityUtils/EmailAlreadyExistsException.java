package com.application.appProject.securityUtils;

public class EmailAlreadyExistsException extends Exception {

	private static final long serialVersionUID = -1403707839474682012L;

	public EmailAlreadyExistsException() {
		super();
	}

	public EmailAlreadyExistsException(String message) {
		super(message);
	}
}
