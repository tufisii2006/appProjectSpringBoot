package com.application.appProject.securityUtils;

public class UserAlreadyExistsException extends Exception {

	private static final long serialVersionUID = -6529042177322677073L;

	public UserAlreadyExistsException() {
		super();
	}

	public UserAlreadyExistsException(String message) {
		super(message);
	}

}
