package com.application.appProject.factory;

import com.application.appProject.abstractModel.User;
import com.application.appProject.mappers.UserMapper;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.restModel.ContractResponse;
import com.application.appProject.restModel.UserResponse;

public class UserResponseFactory {

	/**
	 * Maps a {@link MongoUser} entity to a {@link UserResponse} having an success message
	 * 
	 * @param user
	 * @return {@link UserResponse}
	 */
	public static UserResponse getUserResponseInstance(User user) {
		UserResponse userResponse = UserMapper.mapEntityToResponse(user);
		userResponse.setResponseMessage("Success!");
		return userResponse;
	}

	/**
	 * Returns a  {@link UserResponse} having an error message.
	 * Method used to signal the client that an error occurred during a specific operation
	 * @param errorMessage
	 * @return {@link UserResponse}
	 */
	public static UserResponse getUserResponseErrorInstance(String errorMessage) {
		UserResponse userResponse = new UserResponse();
		userResponse.setResponseMessage(errorMessage);
		return userResponse;
	}

	/**
	 * Returns a  {@link UserResponse} having an success message.
	 * Method used to signal the client that an error occurred during a specific operation
	 * @param successMessage
	 * @return UserResponse
	 */
	public static UserResponse getUserResponseSuccessMessageInstance(String successMessage) {
		UserResponse userResponse = new UserResponse();
		userResponse.setResponseMessage(successMessage);
		return userResponse;
	}
}
