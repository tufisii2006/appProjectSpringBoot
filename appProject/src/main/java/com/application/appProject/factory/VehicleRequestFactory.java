package com.application.appProject.factory;

import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.mappers.VehicleMapper;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.restModel.VehicleRequest;

public class VehicleRequestFactory {

	
	/**
	 * Maps a {@link VehicleRequest} to a {@link MongoVehicle} entity
	 * @param vehicleRequest
	 * @return {@link MongoVehicle}
	 */
	public static Vehicle getVehicleInstanceFromVehicleRequest(VehicleRequest vehicleRequest) {
		Vehicle vehicle = VehicleMapper.mapRequestToEntity(vehicleRequest);
		return vehicle;
	}
}
