package com.application.appProject.factory;

import com.application.appProject.abstractModel.Contract;
import com.application.appProject.mappers.ContractMapper;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.restModel.ContractResponse;

public class ContractResponseFactory {

	/**
	 * Maps a {@link MongoContract} entity to a {@link ContractResponse} having a
	 * success message
	 * 
	 * @param contract
	 * @return {@link ContractResponse}
	 */
	public static ContractResponse getContractResponseInstance(Contract contract) {
		ContractResponse contractResponse = ContractMapper.mapEntityToResponse(contract);
		contractResponse.setResponseMessage("Success!");
		return contractResponse;
	}

	/**
	 * Returns a {@link ContractResponse} having an error message. Method used to
	 * signal the client that an error occurred during an operation
	 * 
	 * @param contract
	 * @return {@link ContractResponse}
	 */
	public static ContractResponse getContractResponseErrorInstance(String errorMessage) {
		ContractResponse contractResponse = new ContractResponse();
		contractResponse.setResponseMessage(errorMessage);
		return contractResponse;
	}

	public static ContractResponse getContractResponseSuccessInstance(String message) {
		ContractResponse contractResponse = new ContractResponse();
		contractResponse.setResponseMessage(message);
		return contractResponse;
	}
}
