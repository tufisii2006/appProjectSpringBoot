package com.application.appProject.factory;

import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.mappers.VehicleMapper;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.restModel.VehicleResponse;

public class VehicleResponseFactory {

	/**
	 * Maps a {@link VehicleResponse} to a {@link MongoVehicle} entity having an success
	 * message
	 * 
	 * @param vehicle
	 * @return {@link VehicleResponse}
	 */
	public static VehicleResponse getVehicleResponseInstance(Vehicle vehicle) {
		VehicleResponse vehicleResponse = VehicleMapper.mapEntityToResponse(vehicle);
		vehicleResponse.setResponseMessage("Success!");
		return vehicleResponse;
	}

	/**
	 * Return a {@link VehicleResponse} having an error message. Method called to
	 * signal the client that an error occurred during a specific operation
	 * 
	 * @param errorMessage
	 * @return {@link VehicleResponse}
	 */
	public static VehicleResponse getVehicleResponseErrorInstance(String errorMessage) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setResponseMessage(errorMessage);
		return vehicleResponse;
	}

	/**
	 * * Return a {@link VehicleResponse} having an success message.
	 * 
	 * @param successMessage
	 * @return {@link VehicleResponse}
	 */
	public static VehicleResponse getVehicleResponseSuccessMessageInstance(String successMessage) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setResponseMessage(successMessage);
		return vehicleResponse;
	}
}
