package com.application.appProject.factory;

import com.application.appProject.mappers.ContractMapper;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.restModel.ContractRequest;

public class ContractRequestFactory {

	/**
	 * Maps a {@link ContractRequest} to a {@link MongoContract} entity
	 * @param contractRequest
	 * @return {@link MongoContract}
	 */
	public static MongoContract getContractInstanceFromContractRequest(ContractRequest contractRequest) {
		MongoContract contract = ContractMapper.mapRequestToEntity(contractRequest);
		return contract;
	}
}
