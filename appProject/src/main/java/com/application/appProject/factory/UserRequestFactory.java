package com.application.appProject.factory;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.abstractModel.User;
import com.application.appProject.mappers.UserMapper;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.restModel.UserRequest;
import com.application.appProject.restModel.UserRequestUpdatePassword;

public class UserRequestFactory {

	/**
	 * Maps a {@link UserRequest} to a {@link MongoUser} entity
	 * 
	 * @param userRequest
	 * @return User
	 */
	public static User getUserInstanceFromUserRequest(UserRequest userRequest) {
		User user = UserMapper.mapRequestToEntity(userRequest);
		return user;
	}

	/**
	 * Maps a {@link UserRequestUpdatePassword} to a {@link MongoUser} entity
	 * 
	 * @param userRequest
	 * @return User
	 */
	public static User getUserUpdatePasswordInstanceFromUserRequest(UserRequestUpdatePassword userRequest) {
		User user = UserMapper.mapUserPasswordRequestToEntity(userRequest);
		return user;
	}
	
	
}
