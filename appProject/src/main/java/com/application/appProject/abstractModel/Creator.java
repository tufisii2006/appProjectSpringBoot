package com.application.appProject.abstractModel;

public enum Creator {
	FORTECH("Fortech"), IOANA("Ioana"), RADU("Radu");

	private final String value;

	Creator(final String newValue) {
		value = newValue;
	}

	public String value() {
		return value;
	}

	/**
	 * Returns the associated {@link Creator} for the given String.
	 * 
	 * @param creatorTypeString
	 * 
	 * @return associated Creator for the given String
	 */
	public static Creator fromString(final String creatorTypeString) {
		for (final Creator creatorType : Creator.values()) {
			if (creatorType.value().equalsIgnoreCase(creatorTypeString)) {
				return creatorType;
			}
		}
		return null;
	}

}
