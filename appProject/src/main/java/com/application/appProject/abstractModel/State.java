package com.application.appProject.abstractModel;

public enum State {
	IN_PREPARATION("BIP"), ACTIVE("BAC"), CANCELED("BCA");

	private String value;

	State(final String newValue) {
		value = newValue;
	}

	public String value() {
		return value;
	}

	/**
	 * Returns the associated {@link State} for the given String.
	 * 
	 * @param stateTypeString
	 * 
	 * @return associated State for the given String
	 */
	public static State fromString(final String stateTypeString) {
		for (final State stateType : State.values()) {
			if (stateType.value().equalsIgnoreCase(stateTypeString)) {
				return stateType;
			}
		}
		return null;
	}
}
