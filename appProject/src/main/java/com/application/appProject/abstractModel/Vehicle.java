package com.application.appProject.abstractModel;

import java.util.Date;

public class Vehicle {
	

	public String getId() {
		return null;
	}

	public void setId(String id) {

	}

	public String getFin() {
		return null;
	}

	public void setFin(String fin) {

	}

	public Branch getBranch() {
		return null;
	}

	public void setBranch(Branch branch) {

	}

	public String getModelYear() {
		return null;
	}

	public void setModelYear(String modelYear) {

	}

	public Brand getBrand() {
		return null;
	}

	public void setBrand(Brand brand) {

	}

	public String getVehicleClass() {
		return null;
	}

	public void setVehicleClass(String vehicleClass) {

	}

	public String getModel() {
		return null;
	}

	public void setModel(String model) {

	}

	public String getTransmissionType() {
		return null;
	}

	public void setTransmissionType(String transmissionType) {

	}

	public String getEngineType() {
		return null;
	}

	public void setEngineType(String engineType) {

	}

	public String getCommisionNumber() {
		return null;
	}

	public void setCommisionNumber(String commisionNumber) {

	}

	public Date getRegistrationDate() {
		return null;
	}

	public void setRegistrationDate(Date registrationDate) {

	}

	public String getEmissionStandard() {
		return null;
	}

	public void setEmissionStandard(String emissionStandard) {

	}

	public Creator getCreatedBy() {
		return null;
	}

	public void setCreatedBy(Creator createdBy) {

	}

	public Creator getModifiedBy() {
		return null;
	}

	public void setModifiedBy(Creator modifiedBy) {

	}

	public Date getCreatedByTimeStamp() {
		return null;
	}

	public void setCreatedByTimeStamp(Date createdByTimeStamp) {

	}

	public Date getModifiedByTimeStamp() {
		return null;
	}

	public void setModifiedByTimeStamp(Date modifiedByTimeStamp) {

	}

}
