package com.application.appProject.abstractModel;

public enum Brand {

	MERCEDES("095"), SMART("126");

	private final String value;

	Brand(final String newValue) {
		value = newValue;
	}

	public String value() {
		return value;
	}

	/**
	 * Returns the associated {@link Brand} for the given String.
	 * 
	 * @param brandTypeString
	 * 
	 * @return associated Brand for the given String
	 */
	public static Brand fromString(final String brandTypeString) {
		for (final Brand brandType : Brand.values()) {
			if (brandType.value().equalsIgnoreCase(brandTypeString)) {
				return brandType;
			}
		}
		return null;
	}

}
