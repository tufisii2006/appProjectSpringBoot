package com.application.appProject.abstractModel;

public enum Portofolio {

	FINANCE_LEASE("0"), OPERATE_LEASE("1"), DIRECT_BUYBACK("2");

	private final String value;

	Portofolio(final String newValue) {
		value = newValue;
	}

	public String value() {
		return value;
	}

	/**
	 * Returns the associated {@link Portofolio} for the given String.
	 * 
	 * @param portofolioTypeString
	 * 
	 * @return associated Portofolio for the given String
	 */
	public static Portofolio fromString(final String portogfolioTypeString) {
		for (final Portofolio portofolioType : Portofolio.values()) {
			if (portofolioType.value().equalsIgnoreCase(portogfolioTypeString)) {
				return portofolioType;
			}
		}
		return null;
	}

}
