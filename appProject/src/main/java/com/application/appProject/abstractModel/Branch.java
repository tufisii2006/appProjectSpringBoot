package com.application.appProject.abstractModel;

public enum Branch {
	CAR("0"), TRUCK("1"), BUS("2");

	private final String value;

	Branch(final String newValue) {
		value = newValue;
	}

	public String value() {
		return value;
	}

	/**
	 * Returns the associated {@link Branch} for the given String.
	 * 
	 * @param branchTypeString
	 * 
	 * @return associated Branch for the given String
	 */
	public static Branch fromString(final String branchTypeString) {
		for (final Branch branchType : Branch.values()) {
			if (branchType.value().equalsIgnoreCase(branchTypeString)) {
				return branchType;
			}
		}
		return null;
	}
}
