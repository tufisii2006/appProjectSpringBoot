package com.application.appProject.abstractModel;

import java.math.BigDecimal;
import java.util.Date;

public class Contract {

	public String getId() {
		return null;
	}

	public void setId(String id) {

	}

	public String getNumber() {
		return null;
	}

	public void setNumber(String number) {

	}

	public State getState() {
		return null;
	}

	public void setState(State state) {

	}

	public Date getStartDate() {
		return null;
	}

	public void setStartDate(Date startDate) {

	}

	public Integer getDuration() {
		return null;
	}

	public void setDuration(Integer duration) {

	}

	public Portofolio getPortofolio() {
		return null;
	}

	public void setPortofolio(Portofolio portofolio) {

	}

	public BigDecimal getRvg() {
		return null;
	}

	public void setRvg(BigDecimal rvg) {

	}

	public BigDecimal getRateSubvention() {
		return null;
	}

	public void setRateSubvention(BigDecimal rateSubvention) {

	}

	public Integer getMileagePerYear() {
		return null;
	}

	public void setMileagePerYear(Integer mileagePerYear) {

	}

	public String getRuGuarantor() {
		return null;
	}

	public void setRuGuarantor(String ruGuarantor) {

	}

	public Date getCreatedTimeStamp() {
		return null;
	}

	public String getCreatedByUserCnp() {
		return null;
	}

	public void setCreatedByUserCnp(String createdByUserCnp) {

	}

	public String getModifiedByUserCnp() {
		return null;
	}

	public void setModifiedByUserCnp(String modifiedByUserCnp) {

	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {

	}

	public Date getModifiedTimeStamp() {
		return null;
	}

	public void setModifiedTimeStamp(Date modifiedTimeStamp) {

	}

	public Integer getContractVersion() {
		return null;
	}

	public void setContractVersion(Integer contractVersion) {

	}

	public Date getCancelationDate() {
		return null;
	}

	public void setCancelationDate(Date cancelationDate) {

	}

	public String getVehicleFin() {
		return null;
	}

	public void setVehicleFin(String vehicleFin) {

	}
}
