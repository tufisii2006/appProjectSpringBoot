package com.application.appProject.mongoRepository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.application.appProject.mongoModel.MongoVehicle;

@Component("VehicleMongoRepo")
public interface VehicleMongoRepository extends MongoRepository<MongoVehicle, String> {

	void deleteByFin(String fin);
	MongoVehicle findByFin(String fin);

}
