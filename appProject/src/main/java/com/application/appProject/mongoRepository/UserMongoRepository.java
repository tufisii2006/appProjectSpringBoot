package com.application.appProject.mongoRepository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.application.appProject.mongoModel.*;

@Component("MongoRepo")
public interface UserMongoRepository extends MongoRepository<MongoUser, String> {

	void deleteByCnp(String cnp);

	MongoUser findByUsernameAndPassword(String username, String password);

	MongoUser findByCnp(String cnp);

	MongoUser findByUsername(String username);
	
	MongoUser findByEmail(String email);
}
