package com.application.appProject.mongoRepository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.application.appProject.mongoModel.*;

@Component("ContractMongoRepo")
public interface ContractRepository extends MongoRepository<MongoContract, String> {

	MongoContract findByNumber(String number);
}
