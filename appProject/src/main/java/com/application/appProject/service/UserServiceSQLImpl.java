package com.application.appProject.service;

import java.util.List;

import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.abstractModel.User;
import com.application.appProject.domain.Role;
import com.application.appProject.securityUtils.UserAlreadyExistsException;
import com.application.appProject.sqlRepo.UserSqlRepository;
import com.application.appProject.validators.UserValidator;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;

@Component("SQLService")
public class UserServiceSQLImpl implements UserService {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	@Qualifier("SQLrepo")
	private UserSqlRepository userRepo;

	@PersistenceContext
	private EntityManager em;

	@Override
	public void insertUser(User user) throws UserAlreadyExistsException {

		if (userRepo.findByCnp(user.getCnp()) != null) {
			throw new UserAlreadyExistsException("The provided cnp is already registred in the database");
		}
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRole(Role.USER.value());
		userRepo.save((SqlUser) user);

	}

	@Override
	@Transactional
	public void deleteUserByCnp(String cnp) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(cnp)) {
			throw new IllegalArgumentException("The cnp is null");
		}
		if (userRepo.findByCnp(cnp) != null) {
			SqlUser userToDelete = em.getReference(SqlUser.class, cnp);
			em.remove(userToDelete);
		} else {
			throw new NoSuchElementException("No User having that CNP exists");
		}
	}

	@Override
	public List<User> getAllUsersFromDb() throws EmptyResultDataAccessException {
		List<SqlUser> allUsersFromDb = userRepo.findAll();
		if (CollectionUtils.isEmpty(allUsersFromDb)) {
			throw new EmptyResultDataAccessException("There are no users in the DB", 1);
		}
		List<User> allUsers = allUsersFromDb.stream().map(x -> (User) x).collect(Collectors.toList());
		return allUsers;
	}

	@Override
	public User getOneUserByCnp(String cnp) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(cnp)) {
			throw new IllegalArgumentException("The cnp is null!");
		}
		SqlUser user = userRepo.findByCnp(cnp);
		if (user == null) {
			throw new NoSuchElementException("No user with that cnp was found!");
		}
		return user;
	}

	@Override
	public void updateUserPassword(User user) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(user.getCnp())) {
			throw new IllegalArgumentException("The cnp is null!");
		}
		User userToUpdate = userRepo.findByCnp(user.getCnp());
		if (userToUpdate == null) {
			throw new NoSuchElementException("No user with that cnp was found!");
		}
		userToUpdate.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepo.save((SqlUser) userToUpdate);
	}

	@Override
	public void updateUserPersonalInfo(User user) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(user.getCnp())) {
			throw new IllegalArgumentException("The cnp is null!");
		}
		User userToUpdate = userRepo.findByCnp(user.getCnp());
		if (userToUpdate == null) {
			throw new NoSuchElementException("No user with that cnp was found!");
		}
		ModelMapper modelMapper = new ModelMapper();
		userToUpdate = modelMapper.map(user, User.class);
		userToUpdate.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepo.save((SqlUser) userToUpdate);
	}

}
