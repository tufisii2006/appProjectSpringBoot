package com.application.appProject.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.application.appProject.mongoModel.MongoUser;

public class UserSessionService {

	public static MongoUser getSessionsUser() {
		return (MongoUser) ((Authentication) SecurityContextHolder.getContext().getAuthentication()).getPrincipal();
	}

	public static WebAuthenticationDetails getConnectionDetailsOfSessionUser() {
		WebAuthenticationDetails connectionDetails = (WebAuthenticationDetails) SecurityContextHolder.getContext()
				.getAuthentication().getDetails();
		return connectionDetails;
	}

}
