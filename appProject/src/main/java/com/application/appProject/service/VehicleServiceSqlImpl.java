package com.application.appProject.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.application.appProject.SQLmodel.SqlVehicle;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.sqlRepo.VehicleSqlRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

@Component("VehicleServiceSql")
public class VehicleServiceSqlImpl implements VehicleService {

	@Autowired
	@Qualifier("VehicleSqlRepo")
	private VehicleSqlRepository vehicleRepo;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Override
	public void insertVehicle(Vehicle vehicle) throws IllegalArgumentException {
		if (vehicle == null) {
			throw new IllegalArgumentException("The vehicle is null");
		}
		vehicleRepo.save((SqlVehicle) vehicle);
	}

	@Override
	public void deleteVehicleFromDb(String id) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(id)) {
			throw new IllegalArgumentException("The vehicle id is null");
		}
		SqlVehicle a = new SqlVehicle();
		a.setId(id);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.contains(a) ? a : entityManager.merge(a));
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public Vehicle getOneVehicleByFin(String fin) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(fin)) {
			throw new IllegalArgumentException("The vehicle id is null");
		}
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query querry = entityManager.createQuery("SELECT v FROM SqlVehicle v WHERE v.fin LIKE :fin").setParameter("fin",
				fin);
		Vehicle vehicle = (Vehicle) querry.getSingleResult();
		if (vehicle == null) {
			throw new NoSuchElementException("No vehicle was found having that fin!");
		}
		return vehicle;
		/*
		 * return (MongoVehicle)
		 * entityManager.createQuery("SELECT v FROM SqlVehicle v WHERE v.name LIKE ?1")
		 * .setParameter(1, fin).getSingleResult();
		 */
	}

	@Override
	public List<Vehicle> getAllVehiclesFromDb() throws EmptyResultDataAccessException {
		List<SqlVehicle> allVehicleFromDb = vehicleRepo.findAll();
		List<Vehicle> allVehicles = allVehicleFromDb.stream().map(x -> (Vehicle) x).collect(Collectors.toList());
		return allVehicles;
	}

	@Override
	public SqlVehicle getOneVehicleWithSpecificFin(String fin) {
		return vehicleRepo.findThatSpecificCar(fin);
	}

	@Override
	public List<SqlVehicle> getAllVehicleWithGivenBrand(Brand brand) {
		return vehicleRepo.getAllVehicleOfGivenBrand(brand);
	}

}
