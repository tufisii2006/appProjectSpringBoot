package com.application.appProject.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.dao.EmptyResultDataAccessException;

import com.application.appProject.SQLmodel.SqlContract;
import com.application.appProject.abstractModel.Contract;
import com.application.appProject.abstractModel.State;
import com.application.appProject.mongoModel.MongoContract;

public interface ContractService {

	List<SqlContract> getFirstFiveContractsOrderByNumber();

	List<SqlContract> customGetAllContracts();

	/**
	 * Inserts a new {@link MongoContract} in the DataBase. The Contract is
	 * validated before inserting
	 * 
	 * @param contract
	 * @throws IllegalArgumentException
	 *             - When the contract is null or it is not valid to be inserted in
	 *             the DataBase
	 */
	void insertNewContract(Contract contract) throws IllegalArgumentException;

	/**
	 * Deletes an {@link MongoContract} entity from the DataBase using its number
	 * 
	 * @param number
	 * @throws IllegalArgumentException
	 *             - When number is null
	 * @throws NoSuchElementException
	 *             - When no {@link MongoContract} entity having that number is
	 *             found
	 */
	void deleteContractByNumber(String number) throws IllegalArgumentException, NoSuchElementException;

	/**
	 * Retrieves all the {@link MongoContract} entities from the DataBase
	 * 
	 * @return List<Contrac>
	 * @throws EmptyResultDataAccessException
	 *             - When no {@link MongoContract} entities are found
	 */
	List<Contract> getAllContracts() throws EmptyResultDataAccessException;

	/**
	 * Change a {@link MongoContract} state {@link State} from IN_PREPARATION to
	 * ACTIVE. The {@link MongoContract} should exist in the DataBase and have the
	 * {@link State} IN_PREPARATION
	 * 
	 * @param contract
	 * @throws IllegalArgumentException
	 *             - When the {@link MongoContract} is null or {@link MongoContract}
	 *             is not in IN_PREPARATION {@link State}
	 * @throws NoSuchElementException
	 *             - When no {@link MongoContract} entity having that number is
	 *             found
	 */
	void activateExistingContract(Contract contract) throws IllegalArgumentException, NoSuchElementException;

	/**
	 * Change a {@link MongoContract} state {@link State} from IN_PREPARATION to
	 * CANCELED. The {@link MongoContract} should exist in the DataBase and have the
	 * {@link State} IN_PREPARATION
	 * 
	 * @param contract
	 * @throws IllegalArgumentException
	 *             - When the {@link MongoContract} is null or {@link MongoContract}
	 *             is not in IN_PREPARATION {@link State}
	 * @throws NoSuchElementException
	 *             - When no {@link MongoContract} entity having that number is
	 *             found
	 */
	void changeContractStateFromIn_PreparationToCanceled(Contract contract)
			throws IllegalArgumentException, NoSuchElementException;

	/**
	 * Change a {@link MongoContract} state {@link State} from ACTIVE to CANCELED.
	 * The {@link MongoContract} should exist in the DataBase and have the
	 * {@link State} ACTIVE
	 * 
	 * @param contract
	 * @throws IllegalArgumentException
	 *             - When the {@link MongoContract} is null or {@link MongoContract}
	 *             is not in IN_PREPARATION {@link State}
	 * @throws NoSuchElementException
	 *             - When no {@link MongoContract} entity having that number is
	 *             found
	 */
	void changeContractStateFromActiveToCanceled(Contract contract)
			throws IllegalArgumentException, NoSuchElementException;

}
