package com.application.appProject.service;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.application.appProject.SQLmodel.SqlContract;
import com.application.appProject.abstractModel.Contract;
import com.application.appProject.abstractModel.State;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.sqlRepo.ContractSqlRepository;
import com.application.appProject.validators.ContractValidator;

@Component("ContractSqlService")
public class ContractServiceSqlImpl implements ContractService {
	@Autowired
	@Qualifier("ContractSqlRepo")
	private ContractSqlRepository contractRepo;

	@Autowired
	@Qualifier("VehicleServiceSql")
	private VehicleService vehicleService;

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<SqlContract> customGetAllContracts() {
		return contractRepo.customGetAllContracts();
	}

	@Override
	public List<SqlContract> getFirstFiveContractsOrderByNumber() {
		return contractRepo.getFirstFiveContractsOrderByNumber(PageRequest.of(0, 4, Direction.ASC, "contract_version"));

	}

	@Override
	@Transactional
	public void insertNewContract(Contract contract) throws IllegalArgumentException, NoSuchElementException {
		if (contract == null) {
			throw new IllegalArgumentException("The contract is not valid to be inserted in DataBase!");
		}
		if (vehicleService.getOneVehicleByFin(contract.getVehicleFin()) == null) {
			throw new NoSuchElementException("The vehicle associated to contract does not exist!");
		}
		contract.setContractVersion(1);
		contract.setState(State.IN_PREPARATION);
		contract.setVehicleFin(contract.getVehicleFin());
		contract.setNumber(contract.getNumber() + contract.getState().value() + contract.getMileagePerYear()
				+ contract.getPortofolio().value()
				+ contract.getVehicleFin().substring(0, Math.min(contract.getVehicleFin().length(), 4)));

		contract.setId(contract.getNumber() + contract.getState().value());
		contract.setCreatedByUserCnp(null);
		contract.setModifiedByUserCnp(null);

		em.persist(contract);

	}

	@Override
	public void deleteContractByNumber(String number) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(number)) {
			throw new IllegalArgumentException("The number is null!");
		}
		SqlContract contractToBeDeleted = contractRepo.findByNumber(number);
		if (contractToBeDeleted == null) {
			throw new NoSuchElementException("No contract having that number was found!");
		}
		contractRepo.delete(contractToBeDeleted);

	}

	@Override
	public List<Contract> getAllContracts() throws EmptyResultDataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void activateExistingContract(Contract contract) throws NoSuchElementException, IllegalArgumentException {
		if (contract == null) {
			throw new IllegalArgumentException("The contract is null!");
		}
		SqlContract contractToBeUpdated = contractRepo.findByNumber(contract.getNumber());
		if (contractToBeUpdated == null) {
			throw new NoSuchElementException("No contract having that number was found!");
		}
		if (contractToBeUpdated.getState() != State.IN_PREPARATION) {
			throw new IllegalArgumentException("Only Contracts having IN_PREPARATION state can be activated!");
		}
		contractToBeUpdated.setContractVersion(2);
		contractToBeUpdated.setState(State.ACTIVE);
		contractToBeUpdated.setStartDate(new Date());
		contractToBeUpdated.setCreatedByUserCnp(contract.getCreatedByUserCnp());
		contractToBeUpdated.setModifiedByUserCnp(contract.getModifiedByUserCnp());
		/*
		 * if(!ContractValidator.isContractValid(contractToBeUpdated)) { throw new
		 * IllegalArgumentException("The contract is not fit to be inserted in DB!");
		 * 
		 * }
		 */
		contractRepo.save(contractToBeUpdated);

	}

	@Override
	public void changeContractStateFromIn_PreparationToCanceled(Contract contract)
			throws IllegalArgumentException, NoSuchElementException {
		// TODO Auto-generated method stub

	}

	@Override
	public void changeContractStateFromActiveToCanceled(Contract contract)
			throws IllegalArgumentException, NoSuchElementException {
		if (contract == null) {
			throw new IllegalArgumentException("The contract is null!");
		}
		SqlContract contractToBeUpdated = contractRepo.findByNumber(contract.getNumber());
		if (contractToBeUpdated == null) {
			throw new NoSuchElementException("No contract having that number was found!");
		}
		if (!ContractValidator.isContractValidToBeCanceledWhenItIsActive(contractToBeUpdated.getStartDate())) {
			throw new IllegalArgumentException(
					"A contract can be canceled only if at least 1 year had passed from the start date!");
		}
		contractToBeUpdated.setCreatedByUserCnp(null);
		contractToBeUpdated.setModifiedByUserCnp(null);
		contractToBeUpdated.setContractVersion(contractToBeUpdated.getContractVersion() + 1);
		contractToBeUpdated.setCancelationDate(new Date());
		contractToBeUpdated.setState(State.CANCELED);
		contractRepo.save(contractToBeUpdated);
	}

}
