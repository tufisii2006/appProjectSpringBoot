package com.application.appProject.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.application.appProject.securityUtils.PdfExport;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

@Component
public class ScheduleService {

	private static String FILE = "C:\\Users\\radu.tufis\\Desktop\\UserPdf.pdf";

	@Autowired
	public EmailService emailSender;

	@Scheduled(fixedRate = 60000)
	public void currentHourAndDate() {
		System.err.println("Ora curenta este");
		System.err.println(new Date());
	}
	
	@Scheduled(cron = "0 0/30 8-22 * * * ", zone = "Europe/Athens")
	public void breakTime() {
		System.err.println("Ar fii indicat sa iei o mica pauza :D!");
		System.err.println(new Date());
	}

	@Scheduled(cron = "0 55 14 * * * ", zone = "Europe/Athens")
	public void dearnotify() throws MessagingException, DocumentException, FileNotFoundException {
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(FILE));
		document.open();
		PdfExport.addMetaData(document);
		PdfExport.addTitlePage(document);
		PdfExport.createTable(document);
		document.close();
		String body = new String("Buna Ioana!\r\n" + "Aici ai un mesaj de la Tufi venit de pe aplicatie ! \r\n"
				+ "Te invit sa verifici attach-amentul :))\r\n" + "   \r\n"
				+ "Acesta este un email automat trimis la ora 14:53 \r\n" + "");
		emailSender.sendMessageWithAttachment("ioana.foidas@fortech.ro", "Schedule from Tufi SpringBootApp", body, FILE);

	}

}
