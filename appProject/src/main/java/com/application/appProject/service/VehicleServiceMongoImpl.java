package com.application.appProject.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.persistence.NamedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.application.appProject.SQLmodel.SqlVehicle;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.User;
import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.mongoModel.MongoVehicle;
import com.application.appProject.mongoRepository.VehicleMongoRepository;
import com.application.appProject.validators.VehicleValidator;

@Component("VehicleServiceMongo")
public class VehicleServiceMongoImpl implements VehicleService {
	
	public static final String FIND_ONE = "SqlVehicle.findThatSpecificCar";
	
	@Autowired
	@Qualifier("VehicleMongoRepo")
	private VehicleMongoRepository vehicleRepo;

	@Override
	public void insertVehicle(Vehicle vehicle) throws IllegalArgumentException {
		if (VehicleValidator.isVehicleValid(vehicle)) {
			vehicleRepo.save((MongoVehicle) vehicle);
		} else {
			throw new IllegalArgumentException("The vehicle is not valid to be inserted in DataBase!");
		}
	}

	@Override
	public void deleteVehicleFromDb(String fin) throws NoSuchElementException, IllegalArgumentException {
		if (StringUtils.isEmpty(fin)) {
			throw new IllegalArgumentException("The fin is null!");
		}
		MongoVehicle vehicleToRemove = vehicleRepo.findByFin(fin);
		if (vehicleToRemove == null) {
			throw new NoSuchElementException("No vehicle having that fin was found!");
		}
		vehicleRepo.delete(vehicleToRemove);
	}

	@Override
	public MongoVehicle getOneVehicleByFin(String fin) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(fin)) {
			throw new IllegalArgumentException("The fin is null!");
		}
		MongoVehicle foundedVehicle = vehicleRepo.findByFin(fin);
		if (foundedVehicle == null) {
			throw new NoSuchElementException("No vehicle having that fin was found!");
		}
		return foundedVehicle;
	}

	@Override
	public List<Vehicle> getAllVehiclesFromDb() throws EmptyResultDataAccessException {
		List<MongoVehicle> allVehiclesFromDb = vehicleRepo.findAll();
		if (CollectionUtils.isEmpty(allVehiclesFromDb)) {
			throw new EmptyResultDataAccessException("There are no vehicles available in the DataBase!", 2);
		}
		List<Vehicle> allVehicles = allVehiclesFromDb.stream().map(x -> (Vehicle) x).collect(Collectors.toList());
		return allVehicles;
	}

	
	@Override
	public Vehicle getOneVehicleWithSpecificFin(String fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SqlVehicle> getAllVehicleWithGivenBrand(Brand brand) {
		// TODO Auto-generated method stub
		return null;
	}

}
