package com.application.appProject.service;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.application.appProject.SQLmodel.SqlContract;
import com.application.appProject.abstractModel.Contract;
import com.application.appProject.abstractModel.State;
import com.application.appProject.abstractModel.User;
import com.application.appProject.mongoModel.MongoContract;
import com.application.appProject.mongoRepository.ContractRepository;
import com.application.appProject.mongoRepository.UserMongoRepository;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.validators.ContractValidator;

@Component("ContractMongoService")
public class ContractServiceImpl implements ContractService {

	@Autowired
	@Qualifier("ContractMongoRepo")
	private ContractRepository contractRepo;

	@Autowired
	@Qualifier("VehicleServiceMongo")
	private VehicleService vehicleService;

	@Override
	public void insertNewContract(Contract contract) throws IllegalArgumentException, NoSuchElementException {
		if (contract == null) {
			throw new IllegalArgumentException("The contract is not valid to be inserted in DataBase!");
		}
		if (vehicleService.getOneVehicleByFin(contract.getVehicleFin()) == null) {
			throw new NoSuchElementException("The vehicle associated to contract does not exist!");
		}

		contract.setContractVersion(1);
		contract.setState(State.IN_PREPARATION);
		contract.setVehicleFin(contract.getVehicleFin());
		contract.setNumber(contract.getNumber() + contract.getState().value() + contract.getMileagePerYear()
				+ contract.getPortofolio().value()
				+ contract.getVehicleFin().substring(0, Math.min(contract.getVehicleFin().length(), 4)));

		contract.setId(contract.getNumber() + contract.getState().value());
		contractRepo.save((MongoContract) contract);
	}

	@Override
	public void changeContractStateFromActiveToCanceled(Contract contract)
			throws IllegalArgumentException, NoSuchElementException {
		if (contract == null) {
			throw new IllegalArgumentException("The contract is null!");
		}
		MongoContract contractToBeUpdated = contractRepo.findByNumber(contract.getNumber());
		if (contractToBeUpdated == null) {
			throw new NoSuchElementException("No contract having that number was found!");
		}
		if (!ContractValidator.isContractValidToBeCanceledWhenItIsActive(contractToBeUpdated.getStartDate())) {
			throw new IllegalArgumentException(
					"A contract can be canceled only if at least 1 year had passed from the start date!");
		}
		contractToBeUpdated.setCreatedByUserCnp(null);
		contractToBeUpdated.setModifiedByUserCnp(null);
		contractToBeUpdated.setContractVersion(contractToBeUpdated.getContractVersion() + 1);
		contractToBeUpdated.setCancelationDate(new Date());
		contractToBeUpdated.setState(State.CANCELED);
		contractRepo.save(contractToBeUpdated);

	}

	@Override
	public void changeContractStateFromIn_PreparationToCanceled(Contract contract)
			throws IllegalArgumentException, NoSuchElementException {
		MongoContract contractToBeUpdated = contractRepo.findByNumber(contract.getNumber());
		ContractValidator.isContractValidToChangeStateFromIn_PreparationToCanceled(contractToBeUpdated);
		if (contractToBeUpdated == null) {
			throw new NoSuchElementException("No contract having that number was found!");
		}
		contractToBeUpdated.setState(State.CANCELED);
		contractToBeUpdated.setContractVersion(contractToBeUpdated.getContractVersion() + 1);
		contractToBeUpdated.setCancelationDate(new Date());
		contractRepo.save(contractToBeUpdated);
	}

	@Override
	public void activateExistingContract(Contract contract) throws NoSuchElementException, IllegalArgumentException {
		if (contract == null) {
			throw new IllegalArgumentException("The contract is null!");
		}
		MongoContract contractToBeUpdated = contractRepo.findByNumber(contract.getNumber());
		if (contractToBeUpdated == null) {
			throw new NoSuchElementException("No contract having that number was found!");
		}
		if (contractToBeUpdated.getState() != State.IN_PREPARATION) {
			throw new IllegalArgumentException("Only Contracts having IN_PREPARATION state can be activated!");
		}
		contractToBeUpdated.setContractVersion(2);
		contractToBeUpdated.setState(State.ACTIVE);
		contractToBeUpdated.setStartDate(new Date());
		contractToBeUpdated.setCreatedByUserCnp(contract.getCreatedByUserCnp());
		contractToBeUpdated.setModifiedByUserCnp(contract.getModifiedByUserCnp());
		/*
		 * if(!ContractValidator.isContractValid(contractToBeUpdated)) { throw new
		 * IllegalArgumentException("The contract is not fit to be inserted in DB!");
		 * 
		 * }
		 */
		contractRepo.save(contractToBeUpdated);

	}

	@Override
	public void deleteContractByNumber(String number) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(number)) {
			throw new IllegalArgumentException("The number is null!");
		}
		MongoContract contractToBeDeleted = contractRepo.findByNumber(number);
		if (contractToBeDeleted == null) {
			throw new NoSuchElementException("No contract having that number was found!");
		}
		contractRepo.delete(contractToBeDeleted);
	}

	@Override
	public List<Contract> getAllContracts() throws EmptyResultDataAccessException {
		List<MongoContract> allContractsFromDb = contractRepo.findAll();
		if (CollectionUtils.isEmpty(allContractsFromDb)) {
			throw new EmptyResultDataAccessException("There are no contracts available in the DataBase!", 2);
		}
		List<Contract> allContracts = allContractsFromDb.stream().map(x -> (Contract) x).collect(Collectors.toList());
		return allContracts;
	}

	@Override
	public List<SqlContract> customGetAllContracts() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SqlContract> getFirstFiveContractsOrderByNumber() {
		// TODO Auto-generated method stub
		return null;
	}

}
