package com.application.appProject.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.dao.EmptyResultDataAccessException;

import com.application.appProject.SQLmodel.SqlVehicle;
import com.application.appProject.abstractModel.Brand;
import com.application.appProject.abstractModel.Vehicle;
import com.application.appProject.mongoModel.MongoVehicle;

public interface VehicleService {
	

	List<SqlVehicle> getAllVehicleWithGivenBrand(Brand brand);
	
	Vehicle getOneVehicleWithSpecificFin(String fin);

	/**
	 * Inserts a new {@link MongoVehicle} in the DataBase.The {@link MongoVehicle} is
	 * validated before insertion
	 * 
	 * @param vehicle
	 * @throws IllegalArgumentException
	 *             - When the vehicle does not satisfy the criteria to be inserted
	 *             or is null
	 */
	void insertVehicle(Vehicle vehicle) throws IllegalArgumentException;

	/**
	 * Deletes a {@link MongoVehicle} entity from DataBase using its fin
	 * 
	 * @param fin
	 * @throws IllegalArgumentException
	 *             - When the fin is null
	 * @throws NoSuchElementException
	 *             - When there is no Vehicle entity having that fin
	 */
	void deleteVehicleFromDb(String fin) throws IllegalArgumentException,NoSuchElementException;

	/**
	 * Retrieve a {@link MongoVehicle} entity from the DataBase using its fin
	 * 
	 * @return {@link MongoVehicle} entity
	 * @throws IllegalArgumentException
	 *             - When the fin is null
	 * @throws NoSuchElementException
	 *             - When no Vehicle entity having that fin is found
	 */
	Vehicle getOneVehicleByFin(String fin) throws IllegalArgumentException, NoSuchElementException;

	/**
	 * Retrieve all the {@link MongoVehicle} entities from the DataBase
	 * 
	 * @return List<Vehicle>
	 * @throws EmptyResultDataAccessException
	 *             - When there are no {@link MongoVehicle} entities in the DataBase
	 */
	List<Vehicle> getAllVehiclesFromDb() throws EmptyResultDataAccessException;

}
