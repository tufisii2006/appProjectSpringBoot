package com.application.appProject.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.application.appProject.abstractModel.User;
import com.application.appProject.domain.Role;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.mongoRepository.UserMongoRepository;
import com.application.appProject.securityUtils.UserAlreadyExistsException;
import com.application.appProject.validators.UserValidator;

@Component("MongoService")
public class UserServiceMongoImpl implements UserService, UserDetailsService {

	@Autowired
	@Qualifier("MongoRepo")
	private UserMongoRepository userRepo;

	private final String userRolePrefix = "ROLE_";
	@Autowired
	private PasswordEncoder passwordEncoder;

	public void insertUser(User user) throws IllegalArgumentException, UserAlreadyExistsException {
		if (UserValidator.isUserValid(user)) {
			if (userRepo.findByCnp(user.getCnp()) != null) {
				throw new UserAlreadyExistsException("The provided cnp is already registred in the database");
			}
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			user.setRole(Role.USER.value());
			userRepo.save((MongoUser) user);
		} else {
			throw new IllegalArgumentException("The user is not valid to be inserted in DataBase!");
		}
	}

	@Override
	public void deleteUserByCnp(String cnp) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(cnp)) {
			throw new IllegalArgumentException("The cnp is null!");
		}
		MongoUser userToRemove = userRepo.findByCnp(cnp);
		if (userToRemove == null) {
			throw new NoSuchElementException("No user having that cnp was found!");
		}
		userRepo.delete(userToRemove);
	}

	@Override
	public List<User> getAllUsersFromDb() throws EmptyResultDataAccessException {
		List<MongoUser> allUsersFromDb = userRepo.findAll();
		if (CollectionUtils.isEmpty(allUsersFromDb)) {
			throw new EmptyResultDataAccessException("There are no users available in the DataBase!", 2);
		}
		List<User> allUsers = allUsersFromDb.stream().map(x -> (User) x).collect(Collectors.toList());
		return allUsers;
	}

	@Override
	public MongoUser getOneUserByCnp(String cnp) throws NoSuchElementException, IllegalArgumentException {
		if (StringUtils.isEmpty(cnp)) {
			throw new IllegalArgumentException("The cnp is null!");
		}
		MongoUser foundedUser = userRepo.findByCnp(cnp);
		if (foundedUser == null) {
			throw new NoSuchElementException("No user having that cnp was found!");
		}
		return foundedUser;
	}

	@Override
	public void updateUserPassword(User newUser) throws IllegalArgumentException, NoSuchElementException {
		if (newUser == null || StringUtils.isEmpty(newUser.getCnp())) {

			throw new IllegalArgumentException("The User or user cnp is null!");
		}
		MongoUser userToGetUpdate = userRepo.findByCnp(newUser.getCnp());
		if (userToGetUpdate == null) {
			throw new NoSuchElementException("No user having that information was founded !");
		}
		userToGetUpdate.setPassword(passwordEncoder.encode(newUser.getPassword()));
		userRepo.save(userToGetUpdate);

	}

	@Override
	public void updateUserPersonalInfo(User user) throws IllegalArgumentException, NoSuchElementException {
		if (user == null || StringUtils.isEmpty(user.getCnp()) == true) {
			throw new IllegalArgumentException("The User or user cnp is null!");
		}
		MongoUser userToGetUpdate = userRepo.findByCnp(user.getCnp());
		if (userToGetUpdate == null) {
			throw new NoSuchElementException("No user having that information was found!");
		}
		userToGetUpdate.setEmail(user.getEmail());
		userToGetUpdate.setFirstName(user.getFirstName());
		userToGetUpdate.setLastName(user.getLastName());
		userToGetUpdate.setPhone(user.getPhone());
		userRepo.save(userToGetUpdate);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		MongoUser user = userRepo.findByUsername(username);
		
		if (user == null) {
			throw new UsernameNotFoundException("Username not found!");
		}
		user.isEnabled();
		user.isAccountNonExpired();
		user.setRole(userRolePrefix + user.getRole());
		return user;
	}

}
