package com.application.appProject.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.abstractModel.User;
import com.application.appProject.mongoModel.MongoUser;
import com.application.appProject.securityUtils.UserAlreadyExistsException;

/**
 * Handles CRUD operations on {@link MongoUser} entity
 *
 */
public interface UserService {

	/**
	 * Inserts a new {@link MongoUser} in the DataBase.The {@link MongoUser} is validated
	 * before insertion
	 * 
	 * @param user
	 * @throws IllegalArgumentException
	 *             - when the User is null or does not satisfy a valid user criteria
	 * @throws NoSuchAlgorithmException 
	 */
	void insertUser(User user) throws IllegalArgumentException,UserAlreadyExistsException ;

	/**
	 * Deletes an existing User from the DataBased using the User cnp
	 * 
	 * @param cnp
	 * @throws IllegalArgumentException
	 *             - When User cnp is null
	 * @throws NoSuchElementException
	 *             - When no User having the input CNP was found in the database
	 */
	void deleteUserByCnp(String cnp) throws IllegalArgumentException, NoSuchElementException;

	/**
	 * Returns a List containing all the Users from the DataBase
	 * 
	 * @return List<User>
	 * @throws EmptyResultDataAccessException
	 *             - When there are no Users in the DataBase
	 */
	List<User> getAllUsersFromDb() throws EmptyResultDataAccessException;

	/**
	 * Returns a User from the DataBase having the input cnp
	 * 
	 * @param cnp
	 * @return User
	 * @throws IllegalArgumentException
	 *             - When cnp is null
	 * @throws NoSuchElementException
	 *             - When no User having the input cnp is found
	 */
	User getOneUserByCnp(String cnp) throws IllegalArgumentException, NoSuchElementException;

	/**
	 * Updates the password of an existing User from the DataBase
	 * 
	 * @param user
	 * @throws IllegalArgumentException
	 *             - When the input User is null
	 * @throws NoSuchElementException
	 *             - When no User to be updated is found
	 * @throws IllegalArgumentException
	 *             - When the new User password are not satisfying constraints
	 * @throws NoSuchAlgorithmException 
	 */
	void updateUserPassword(User user) throws IllegalArgumentException, NoSuchElementException;

	/**
	 * Updates the personal information of an existing User from the DataBase
	 * 
	 * @param user
	 * @throws IllegalArgumentException
	 *             - When the input User is null
	 * @throws NoSuchElementException
	 *             - When no User to be updated is found
	 * @throws IllegalArgumentException
	 *             - When the new User information does not satisfying constraints
	 */
	void updateUserPersonalInfo(User user) throws IllegalArgumentException, NoSuchElementException;

}
