package com.application.appProject.sqlRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.application.appProject.SQLmodel.SqlUser;

@Component("SQLrepo")
public interface UserSqlRepository extends JpaRepository<SqlUser,String> {
	
	void deleteByCnp(String cnp);

	SqlUser findByUsernameAndPassword(String username, String password);

	SqlUser findByCnp(String cnp);

	SqlUser findByUsername(String username);
	
	SqlUser findByEmail(String email);
}
