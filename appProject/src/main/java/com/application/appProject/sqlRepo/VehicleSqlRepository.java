package com.application.appProject.sqlRepo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.application.appProject.SQLmodel.SqlUser;
import com.application.appProject.SQLmodel.SqlVehicle;
import com.application.appProject.abstractModel.Brand;

@Component("VehicleSqlRepo")
public interface VehicleSqlRepository extends JpaRepository<SqlVehicle, String> {
	@Query(name = "SqlVehicle.findThatSpecificCar")
	SqlVehicle findThatSpecificCar(@Param("fin") String fin);

	@Query(name = "SqlVehicle.getAllVehicleOfGivenBrand")
	List<SqlVehicle> getAllVehicleOfGivenBrand(@Param("brand") Brand brand);
	
}
