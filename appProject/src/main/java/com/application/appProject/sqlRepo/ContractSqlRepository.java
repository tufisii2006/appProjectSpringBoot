package com.application.appProject.sqlRepo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.application.appProject.SQLmodel.SqlContract;

@Component("ContractSqlRepo")
public interface ContractSqlRepository extends JpaRepository<SqlContract, String> {

	@Query(name = "SqlContract.customGetAllContracts")
	List<SqlContract> customGetAllContracts();

	@Query(name = "SqlContract.getFirstFiveContractsOrderByNumber")
	List<SqlContract> getFirstFiveContractsOrderByNumber(Pageable pageable);

	SqlContract findByNumber(String number);
}
